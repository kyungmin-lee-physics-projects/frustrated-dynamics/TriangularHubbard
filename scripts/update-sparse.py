import os, sys
import sqlite3
import glob
import lzma
import msgpack
import msgpack_numpy
import numpy as np
import argparse

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("directory", type=str, default=".")
    args = parser.parse_args()
    directory = args.directory

    con = sqlite3.connect(os.path.join(directory, "sectors.sqlite3"))
    cur = con.cursor()

    for filename in os.listdir(os.path.join(directory, "sparsedata")):
        if not os.path.isfile(os.path.join(directory, "sparsedata", filename)):
            continue

        cur.execute("SELECT COUNT(1) FROM sparse_results WHERE filename=?", (filename,))
        (c, ) = cur.fetchone()
        if c != 1:
            print("file {} not in the database (count = {})".format(filename, c))
            continue

        with lzma.open(os.path.join(directory, "sparsedata", filename)) as xzf:
            data = msgpack.unpack(xzf, object_hook=msgpack_numpy.decode)
        lengths = np.unique([len(zs) for zs in data["partition"]])
        if len(lengths) != 1:
            print("file {} corrupt. lengths of partitions not equal".format(filename))
            continue
        l = lengths[0]
        cur.execute("SELECT filename, samplecount FROM sparse_results WHERE filename=?", (filename,))
        (f, s) = cur.fetchone()
        if s != l:
            print("Updating db on {}".format(filename))
            cur.execute("UPDATE sparse_results SET samplecount=? WHERE filename=?", (l, filename))

    cur.close()
    con.close()

if __name__=='__main__':
    main()
