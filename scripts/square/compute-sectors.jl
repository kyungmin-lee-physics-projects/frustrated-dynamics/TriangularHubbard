using DrWatson
@quickactivate "TriangularHubbard"

using TriangularHubbard

using LinearAlgebra
using Random
using LatticeTools
using QuantumHamiltonian
using QuantumHamiltonianParticle

using Formatting
using Logging
using ArgParse

using SQLite
using DBInterface
using ProgressMeter



function compute_sectors(shape::AbstractMatrix{<:Integer})
    BR = UInt
    shape_str = shape_string(shape)

    # Set up lattice
    square = make_square_lattice(shape)
    n_sites = numsites(square.lattice.supercell)

    lattice = square.lattice
    ssymbed = square.space_symmetry_embedding
    tsymbed = ssymbed.normal
    psymbed = ssymbed.rest

    # Set up particle sector and Hilbert space
    ps, c, cdag = electron_system()

    em = ParticleState(ps, "em", [0, 0], (0, 0))
    up = ParticleState(ps, "up", [1, 0], (1, 0))
    dn = ParticleState(ps, "dn", [0, 1], (0, 1))
    ud = ParticleState(ps, "ud", [1, 1], (1, 1))
    site = ParticleSite([em, up, dn, ud])

    hs = ParticleHilbertSpace([site for i in 1:n_sites])

    ssa_collection = []
    for tii in 1:irrepcount(tsymbed)
        tsa = collect(get_irrep_iterator(IrrepComponent(tsymbed, tii)))
        psymbed_little = little_symmetry(tsymbed, tii, psymbed)
        for pii in 1:num_irreps(psymbed_little), pic in 1:irrep_dimension(psymbed_little, pii)
            psa = get_irrep_iterator(IrrepComponent(psymbed_little, pii, pic))
            ssa = collect(make_product_irrep(psa, tsa))
            push!(ssa_collection, ((tii, pii, pic), ssa))
        end
    end

    idx = 0
    total_dimension = 0
    sectors = let
        tmp = Dict(qn => [] for qn in quantum_number_sectors(hs))
        nqns = length(quantum_number_sectors(hs))
        lck = Threads.SpinLock()
        prog = Progress(4^n_sites)
        processed_qns = 0
        Threads.@threads for qn in quantum_number_sectors(hs)
            nup, ndn = qn
            hsr = represent(hs, [qn], BR)
            local_total_dimension = 0
            for ((tii, pii, pic), ssa) in ssa_collection
                rhsr = symmetry_reduce(hsr, [s for (s, p) in ssa], [p for (s, p) in ssa])
                local_total_dimension += dimension(rhsr)
                push!(tmp[qn], (nup=nup, ndn=ndn, tii=tii, pii=pii, pic=pic, dim=dimension(rhsr)))
            end
            lock(lck)
            total_dimension += local_total_dimension
            processed_qns += 1
            ProgressMeter.update!(prog, total_dimension; showvalues=[(:D, "$(total_dimension)/$(4^n_sites)"), (:qn, "$(processed_qns)/$(nqns)")])
            unlock(lck)
        end # for qn

        T = typeof((idx=1,nup=1,ndn=1,tii=1,pii=1,pic=1,dim=1))
        sectors = T[]
        sizehint!(sectors, length(ssa_collection) * length(quantum_number_sectors(hs)))

        idx = 0
        for qn in sort(collect(keys(tmp))), x in tmp[qn]
            idx += 1
            nup, ndn = qn
            push!(sectors, (idx=idx, nup=x.nup, ndn=x.ndn, tii=x.tii, pii=x.pii, pic=x.pic, dim=x.dim))
        end
        sectors
    end

    println("total dimension: ", sum(x.dim for x in sectors))
    db = DBInterface.connect(SQLite.DB, datadir("square", "sectors-$shape_str.sqlite3"))
    results = DBInterface.execute(db, "SELECT name FROM sqlite_master WHERE type=? AND name=?", ("table", "sectors"))
    if isempty(results)
        DBInterface.execute(db, """
        CREATE TABLE sectors (
            idx INTEGER NOT NULL PRIMARY KEY,
            nup INTEGER NOT NULL,
            ndn INTEGER NOT NULL,
            tii INTEGER NOT NULL,
            pii INTEGER NOT NULL,
            pic INTEGER NOT NULL,
            dim INTEGER NOT NULL,
            UNIQUE(nup, ndn, tii, pii, pic)
        )""")
    end

    sectors_transposed = (
        idx=[x.idx for x in sectors],
        nup=[x.nup for x in sectors],
        ndn=[x.ndn for x in sectors],
        tii=[x.tii for x in sectors],
        pii=[x.pii for x in sectors],
        pic=[x.pic for x in sectors],
        dim=[x.dim for x in sectors],
    )

    @info "Writing to database"
    DBInterface.executemany(db, "INSERT OR REPLACE INTO sectors VALUES (?, ?, ?, ?, ?, ?, ?)", sectors_transposed)
    DBInterface.close!(db)
end # function compute


function parse_commandline()
    s = ArgParse.ArgParseSettings()
    @add_arg_table! s begin
        "shape"
            arg_type = String
            help = "shape of the lattice in the format (?,?)x(?,?)"
            required = true
    end
    parse_args(s)
end


function main()
    parsed_args = parse_commandline()
    shape = parse_shape(parsed_args["shape"])
    compute_sectors(shape)
end


main()

