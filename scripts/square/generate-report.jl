using DrWatson
@quickactivate "TriangularHubbard"

using TriangularHubbard
using ArgParse

function generate_report(
    shape::AbstractMatrix{<:Integer},
)
    square = make_square_lattice(shape)
    shape_str = shape_string(shape)
    output_filepath = datadir("square", "lattice-$(shape_str).json")
    write_lattice_yaml(square.lattice, "4mm", output_filepath)
end

function parse_commandline()
    s = ArgParse.ArgParseSettings()
    @add_arg_table! s begin
        "shape"
            arg_type = String
            help = "shape of the lattice in the format (?,?)x(?,?)"
            required = true
    end
    parse_args(s)
end

function main()
    parsed_args = parse_commandline()
    shape = parse_shape(parsed_args["shape"])
    generate_report(shape)
end

main()
