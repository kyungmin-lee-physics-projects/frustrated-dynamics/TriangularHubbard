using DrWatson
@quickactivate "TriangularHubbard"

using TriangularHubbard

using LinearAlgebra
using Random

using LatticeTools
using KrylovKit
using QuantumHamiltonian
using QuantumHamiltonianParticle
using FiniteTemperatureLanczos

using DataStructures
using CodecXz
using MsgPack
using Formatting

using Logging
using ArgParse
using ProgressMeter

using JSON
using SQLite
using DBInterface
# using CSV
using DataFrames

using UUIDs

try
  @eval using MKL
catch
end

const SectorType = NamedTuple{(:idx, :nup, :ndn, :tii, :pii, :pic), Tuple{Int, Int, Int, Int, Int, Int}}

# include("serialize.jl")

function process_sparse(
    shape::AbstractMatrix{<:Integer},
    t::Real,
    U::Real,
    temperatures::AbstractVector{<:Real},
    sector_indices::AbstractVector{<:Integer};
    seed::Integer=0,
    krylovdim::Integer=200,
    nblocks::Integer=100,
    blocksize::Integer=10,
    nsamples::Integer=1000,
)
    shape_str = shape_string(shape)
    sectors_filepath = datadir("square", "curie", shape_str, "sectors.sqlite3")
    conn = initialize_database(sectors_filepath)

    all_sectors = [(idx=x.idx, nup=x.nup, ndn=x.ndn, tii=x.tii, pii=x.pii, pic=x.pic) for x in DBInterface.execute(conn, """
        SELECT sectors.idx AS idx, nup, ndn, tii, pii, pic
        FROM sectors
        INNER JOIN schedule ON sectors.idx = schedule.idx
    """)]
    sectors = [x for x in all_sectors if x.idx in sector_indices]
    DBInterface.close!(conn)

    return process_sparse(shape, t, U, temperatures, sectors;
                          seed=seed, krylovdim=krylovdim, nblocks=nblocks, blocksize=blocksize, nsamples=nsamples)
end



function process_sparse(
    shape::AbstractMatrix{<:Integer},
    t::Real,
    U::Real,
    temperatures::AbstractVector{<:Real},
    sectors::AbstractVector{SectorType}
    ;
    seed::Integer=0,
    krylovdim::Integer=200,
    nblocks::Integer=100,
    blocksize::Integer=10,
    nsamples::Integer=1000,
)
    BR = UInt

    # Set up lattice
    square = make_square_lattice(shape)

    lattice = square.lattice
    ssymbed = square.space_symmetry_embedding
    tsymbed = ssymbed.normal
    psymbed = ssymbed.rest

    hs, hamiltonian, c, cdag = make_hamiltonian_ladder(square, t, U)
    @mylogmsg "Finished making Hamiltonian"

    pair_groups = Vector{Tuple{Int, Int}}[]
    let visited = falses(numsites(lattice.supercell), numsites(lattice.supercell))
        for (i1, (sitename1, sitefc1)) in enumerate(lattice.supercell.sites),
            (i2, (sitename2, sitefc2)) in enumerate(lattice.supercell.sites)
            visited[i1, i2] && continue
            pair_group = Set()
            for t in tsymbed.elements, p in psymbed.elements
                j1 = p(t(i1))
                j2 = p(t(i2))
                push!(pair_group, (j1, j2))
                visited[j1, j2] = true
            end
            push!(pair_groups, sort(collect(pair_group)))
        end
    end

    nup(i::Integer) = cdag(i, :up) * c(i, :up)
    ndn(i::Integer) = cdag(i, :dn) * c(i, :dn)

    correlation = []

    @info "pair groups: $(pair_groups)"

    for pair_group in pair_groups
        push!(correlation, sum(nup(i) * nup(j) for (i, j) in pair_group) / length(pair_group))
        push!(correlation, sum(nup(i) * ndn(j) for (i, j) in pair_group) / length(pair_group))
        push!(correlation, sum(ndn(i) * nup(j) for (i, j) in pair_group) / length(pair_group))
        push!(correlation, sum(ndn(i) * ndn(j) for (i, j) in pair_group) / length(pair_group))
    end

    @assert all(isinvariant(x, y) for x in tsymbed.elements for y in correlation)
    @assert all(isinvariant(x, y) for x in psymbed.elements for y in correlation)

    correlation = [make_projector_operator(op, BR) for op in correlation]

    prev_idx = nothing
    rhsr = nothing

    prev_nupndn = nothing
    hsr = nothing

    prev_tii = nothing
    tsa = nothing
    psymbed_little = nothing

    prev_tiipiipic = nothing
    psa = nothing
    ssa = nothing

    @mylogmsg "Opening DB file"

    shape_str = shape_string(shape)

    for (idx, nup, ndn, tii, pii, pic) in sectors
        @mylogmsg "Sector: idx=$idx, nup=$nup, nn=$ndn, tii=$tii, pii=$pii, pic=$pic"

        output_filename = savename("sparse", Dict(:idx=>idx, :t=>t, :U=>U, :krylovdim=>krylovdim, :seed=>seed), "mxz")
        output_filepath = datadir("square", "curie", shape_str, "sparsedata", output_filename)

        if ispath(output_filepath)
            @mylogmsg "Read existing data"
            data = open(output_filepath, "r") do io
                ioc = XzDecompressorStream(io)
                data = MsgPack.unpack(ioc)
                close(ioc)
                close(io)
                return data
            end
            base_energy = data["base_energy"]
            zs = data["partition"]
            Es = data["energy"]
            E2s = data["energy_squared"]
            obs = data["correlation"]
            rng = mydeserialize(MersenneTwister, data["rng"])
            samplecount = only(unique([length(z) for z in zs]))
        else
            @mylogmsg "Starting afresh"
            rng = MersenneTwister(seed)
            samplecount = 0
            base_energy = nothing
            zs = [Float64[] for T in temperatures]
            Es = [Float64[] for T in temperatures]
            E2s = [Float64[] for T in temperatures]
            obs = [[Float64[] for T in temperatures] for _ in correlation]
        end
        @mylogmsg "rng: $rng"
        @mylogmsg "samplecount: $samplecount"
        if samplecount >= nsamples
            @mylogmsg "samplecount $samplecount ≥ nsamples $nsamples"
            continue
        end
        @mylogmsg "base_energy: $base_energy"
        GC.gc()
        @mylogmsg "Garbage collected"

        if idx == prev_idx
            @mylogmsg "Using previous sector rhsr"
            @assert !isnothing(rhsr)
        else
            if (nup, ndn) == prev_nupndn
                @mylogmsg "Using previous hsr"
                @assert !isnothing(hsr)
            else
                hsr = represent(hs, [(nup, ndn),], BR, DictIndexedVector)
                prev_nupndn = (nup, ndn)
                @mylogmsg "Finished creating hsr"
            end
            if tii == prev_tii
                @mylogmsg "Using previous tii"
                @assert !isnothing(tsa)
                @assert !isnothing(psymbed_little)
            else
                tsa = collect(get_irrep_iterator(IrrepComponent(tsymbed, tii)))
                psymbed_little = little_symmetry(tsymbed, tii, psymbed)
                prev_tii = tii
            end
            if (tii, pii, pic) == prev_tiipiipic
                @mylogmsg "Using previous psa, ssa"
                @assert !isnothing(psa)
                @assert !isnothing(ssa)
            else
                psa = collect(get_irrep_iterator(IrrepComponent(psymbed_little, pii, pic)))
                ssa = make_product_irrep(psa, tsa)
                prev_tiipiipic = (tii, pii, pic)
            end
            rhsr = symmetry_reduce(hsr, ssa)
            @mylogmsg "Finished creating rhsr"
        end
        D = dimension(rhsr)
        if D == 0
            @mylogmsg "Sector has zero dimension. Skip."
            continue
        end

        @mylogmsg "Generating Hamiltonian representation"
        h = represent(rhsr, hamiltonian)

        @mylogmsg "Generating observables representation"
        observables = [Observable(represent(rhsr, ss)) for ss in correlation]

        @mylogmsg "Start computing"
        for iblock in 1:nblocks
            if samplecount >= nsamples
                @mylogmsg "samplecount $samplecount ≥ nsamples $nsamples"
                break
            end
            @mylogmsg "Running block $iblock"
            ret = try
                ret = compute_thermodynamics_sparse(
                    h, temperatures, observables;
                    rng=rng, krylovdim=krylovdim, nsamples=blocksize,
                    base_energy=base_energy,
                )
            catch 
                @show h
                v = ones(ComplexF64, size(h, 1))
                @show h * v
                rethrow()
            end
            samplecount += blocksize
            base_energy = ret.base_energy
            @mylogmsg "base_energy: $base_energy"

            for (iT, T) in enumerate(temperatures)
                append!(zs[iT], ret.z[iT])
                append!(Es[iT], ret.E[iT])
                append!(E2s[iT], ret.E2[iT])
                for iobs in eachindex(observables)
                    append!(obs[iobs][iT], real(ret.obs[iobs][iT]))
                end
            end

            @mylogmsg "Saving block $iblock ($samplecount samples)"
            # @mylogmsg "rng: $rng"
            isdir(dirname(output_filepath)) || mkpath(dirname(output_filepath))
            open("$output_filepath.new", "w") do io
                ioc = XzCompressorStream(io)
                pack(ioc, OrderedDict(
                            "hopping" => t,
                            "interaction" => U,
                            "idx" => idx,
                            "krylovdim" => krylovdim,
                            "seed" => seed,
                            "rng" => myserialize(rng),

                            "base_energy" => base_energy,
                            "temperature" => temperatures,
                            "partition" => zs,
                            "energy" => Es,
                            "energy_squared" => E2s,
                            "correlation" => obs,
                ))
                close(ioc)
                close(io)
            end
            mv("$output_filepath.new", output_filepath, force=true)
        end # for iblock
    end
end


function parse_commandline()
    s = ArgParse.ArgParseSettings()
    @add_arg_table! s begin
        "shape"
            arg_type = String
            help = "shape of the lattice in the format (?,?)x(?,?)"
            required = true
        "--hopping", "-t"
            arg_type = Float64
            default = 1.0
        "--interaction", "-U"
            arg_type = Float64
            required = true
        "--temperature", "-T"
            arg_type = Float64
            nargs = '+'
            required = true
        "--sector"
            arg_type = Int
            nargs = '+'
            required = true
        "--krylovdim"
            arg_type = Int
            range_tester = x -> x > 0
            default = 200
        "--nblocks"
            arg_type = Int
            range_tester = x -> x > 0
            default = 100
        "--blocksize"
            arg_type = Int
            range_tester = x -> x > 0
            default = 10
        "--nsamples"
            arg_type = Int
            range_tester = x -> x > 0
            default = 1000
        "--seed"
            arg_type = Int
            default = 0
        "--force", "-f"
            action = :store_true
        "--debug", "-d"
            action = :store_true
    end
    parse_args(s)
end


function main()
    parsed_args = parse_commandline()
    if parsed_args["debug"]
        logger = ConsoleLogger(stdout, Logging.Debug; meta_formatter=my_metafmt)
        global_logger(logger)
    else
        logger = ConsoleLogger(stdout, Logging.Info; meta_formatter=my_metafmt)
        global_logger(logger)
    end

    shape = parse_shape(parsed_args["shape"])
    t = parsed_args["hopping"]
    U = parsed_args["interaction"]
    temperatures = parsed_args["temperature"]
    seed = parsed_args["seed"]
    krylovdim = parsed_args["krylovdim"]
    nblocks = parsed_args["nblocks"]
    blocksize = parsed_args["blocksize"]
    nsamples = parsed_args["nsamples"]

    sectors = parsed_args["sector"]


    @mylogmsg "Starting $(first(ARGS))"
    @mylogmsg "shape: $shape"
    @mylogmsg "t: $t"
    @mylogmsg "U: $U"
    @mylogmsg "T: $temperatures"
    @mylogmsg "seed: $seed"
    @mylogmsg "krylovdim: $krylovdim"
    @mylogmsg "nblocks: $nblocks"
    @mylogmsg "blocksize: $blocksize"
    @mylogmsg "nsamples: $nsamples"
    @mylogmsg "sectors: $sectors"

    process_sparse(shape, t, U, temperatures, sectors; seed=seed, krylovdim=krylovdim, nblocks=nblocks, blocksize=blocksize, nsamples=nsamples)

    @mylogmsg "Finished writing"
end


main()

