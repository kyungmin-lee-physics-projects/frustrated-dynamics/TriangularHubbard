using DrWatson
@quickactivate "TriangularHubbard"

# using QuantumHamiltonian
# using QuantumHamiltonianParticle
# using LatticeTools
# using FiniteTemperatureLanczos
using ArgParse
using TriangularHubbard

using DBInterface
using SQLite

function initialize_database(sectors_filepath::AbstractString)
    conn = DBInterface.connect(SQLite.DB, sectors_filepath)
    r = DBInterface.execute(conn, "SELECT * FROM sqlite_master WHERE name=?", ("sectors",))
    if isempty(r)
        throw(ArgumentError("file $sectors_filepath does not contain table sectors"))
    end

    r = DBInterface.execute(conn, "SELECT * FROM sqlite_master WHERE name=?", ("schedule",))
    if isempty(r)
        DBInterface.execute(conn, """
        CREATE TABLE schedule (
            idx INTEGER NOT NULL PRIMARY KEY,
            type TEXT,
            filepath TEXT
        )""")
    end
    return conn
end

function schedule_sectors(shape::AbstractMatrix{<:Integer}, lo::Integer, hi::Integer)
    @assert 0 < lo < hi
    shape_str = shape_string(shape)

    sectors_filepath = datadir("square", "curie", shape_str, "sectors.sqlite3")
    if !ispath(sectors_filepath)
        sectors_directory = dirname(sectors_filepath)
        isdir(sectors_directory) || mkpath(sectors_directory)
        cp(datadir("square", "sectors-$shape_str.sqlite3"), sectors_filepath)
    end
    conn = initialize_database(sectors_filepath)
    r = DBInterface.execute(conn, """
        INSERT OR REPLACE INTO schedule (idx, type)
        SELECT idx, 'small' AS type FROM sectors WHERE 0 < dim AND dim <= ?
    """, (lo, ))

    r = DBInterface.execute(conn, """
        INSERT OR REPLACE INTO schedule (idx, type)
        SELECT idx, 'dense' AS type FROM sectors WHERE ? < dim AND dim <= ?
    """, (lo, hi))

    r = DBInterface.execute(conn, """
        INSERT OR REPLACE INTO schedule (idx, type)
        SELECT idx, 'sparse' AS type FROM sectors WHERE ? < dim
    """, (hi,))
end

function parse_commandline()
    s = ArgParse.ArgParseSettings()
    @add_arg_table! s begin
        "shape"
            arg_type = String
            help = "shape of the lattice in the format (?,?)x(?,?)"
            required = true
        "lo"
            arg_type = Int
            required = true
            help = "0 < dim <= lo are small"
        "hi"
            arg_type = Int
            required = true
            help = "lo < dim <= hi are dense, hi < dim are sparse"
    end
    return parse_args(s)
end

function main()
    parsed_args = parse_commandline()
    shape = parse_shape(parsed_args["shape"])
    lo = parsed_args["lo"]
    hi = parsed_args["hi"]
    schedule_sectors(shape, lo, hi)
end

main()
