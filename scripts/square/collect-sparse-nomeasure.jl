using DrWatson
@quickactivate "TriangularHubbard"

using ArgParse
using DBInterface
using SQLite
using MsgPack
using CodecXz
using JSON
using ProgressMeter
using Glob
using Statistics

function parse_commandline()
    s = ArgParse.ArgParseSettings()
    @add_arg_table! s begin
        "shape"
            arg_type = String
            help = "shape of the lattice in the format (?,?)x(?,?)"
            required = true
    end
    parse_args(s)
end

function main()
    parsed_args = parse_commandline()
    shape_str = parsed_args["shape"]

    conn = DBInterface.connect(SQLite.DB, datadir("square", "curie-nomeasure", shape_str, "sectors.sqlite3"))
    SQL(args...; kwargs...) = DBInterface.execute(conn, args...; kwargs...)

    dimensions = Dict{Int, Int}()
    for row in SQL("SELECT idx, dim FROM sectors")
        dimensions[row.idx] = row.dim
    end

    SQL("BEGIN")
    SQL("""
        CREATE TABLE IF NOT EXISTS sparse_results (
            idx INTEGER NOT NULL,
            hopping REAL NOT NULL,
            interaction REAL NOT NULL,
            temperature REAL NOT NULL,
            partition REAL NOT NULL,
            energy REAL NOT NULL,
            energy_squared REAL NOT NULL,
            correlation TEXT,
            samplecount INTEGER NOT NULL,
            UNIQUE(idx, hopping, interaction, temperature)
        )"""
    )

    SQL("""
        CREATE TABLE IF NOT EXISTS sparse_energy_shifts (
            idx INTEGER NOT NULL,
            hopping REAL NOT NULL,
            interaction REAL NOT NULL,
            base_energy REAL NOT NULL,
            UNIQUE(idx, hopping, interaction),
            FOREIGN KEY(idx) REFERENCES sectors(idx)
        )
    """)

    lk = Threads.SpinLock()
    filepaths = Glob.glob("sparse*.mxz", datadir("square", "curie-nomeasure", shape_str, "sparsedata"))
    p = Progress(length(filepaths))
    @info "Threads: $(Threads.nthreads())"

    threadlocal_sparse_energy_shifts = [[] for i in 1:Threads.nthreads()]
    threadlocal_sparse_results = [[] for i in 1:Threads.nthreads()]

    Threads.@threads for filepath in filepaths
        data = open(filepath, "r") do io
            ioc = XzDecompressorStream(io)
            data = MsgPack.unpack(ioc)
            close(ioc)
            close(io)
            data
        end
        idx = Int(data["idx"])
        hopping = Float64(data["hopping"])
        interaction = Float64(data["interaction"])
        base_energy = Float64(data["base_energy"])

        lock(lk)
        try
            SQL("""
                INSERT OR REPLACE INTO sparse_energy_shifts
                (idx, hopping, interaction, base_energy)
                VALUES (?, ?, ?, ?)
            """, (idx, hopping, interaction, base_energy))
        finally
            unlock(lk)
        end
        #push!(threadlocal_sparse_energy_shifts[Threads.threadid()], (idx, hopping, interaction, base_energy))

        dim = dimensions[idx]

        for (iT, T) in enumerate(data["temperature"])
            partitions = data["partition"][iT]
            energies = data["energy"][iT]
            energies_squared = data["energy_squared"][iT]
            #correlation = [x[iT] for x in data["correlation"]]

            z = mean(partitions) * dim
            E = mean(energies) * dim
            E2 = mean(energies_squared) * dim        
            #Sx = mean.(correlation) .* dim
            #Sx_str = JSON.json(Sx)
            Sx_str = ""
            
            lock(lk)
            try
                SQL("""
                    INSERT OR REPLACE INTO sparse_results
                    (idx, hopping, interaction, temperature, partition, energy, energy_squared, correlation, samplecount)
                    VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)
                """, (idx, hopping, interaction, T, z, E, E2, Sx_str, length(partitions)))
            finally
                unlock(lk)
            end
            #push!(threadlocal_sparse_results[Threads.threadid()], (idx, hopping, interaction, T, z, E, E2, Sx_str, length(partitions)))
        end
        next!(p)
    end
    SQL("COMMIT")
    DBInterface.close!(conn)
end

main()
