using DrWatson
@quickactivate "TriangularHubbard"

using TriangularHubbard

using LinearAlgebra
using Random

using LatticeTools
using KrylovKit
using QuantumHamiltonian
using QuantumHamiltonianParticle
using FiniteTemperatureLanczos

using DataStructures
using CodecXz
using MsgPack
using Formatting

using Logging
using ArgParse
using ProgressMeter

using JSON
using SQLite
using DBInterface
# using CSV
using DataFrames

using UUIDs

try
  @eval using MKL
catch
end

const SectorType = NamedTuple{(:idx, :nup, :ndn, :tii, :pii, :pic), Tuple{Int, Int, Int, Int, Int, Int}}


function process_small(
    shape::AbstractMatrix{<:Integer},
    t::Real,
    U::Real,
    temperatures::AbstractVector{<:Real},
    ;
    commitevery::Integer=1
)
    shape_str = shape_string(shape)
    sectors_filepath = datadir("square", "curie", shape_str, "sectors.sqlite3")
    conn = initialize_database(sectors_filepath)
    sectors = [(idx=x.idx, nup=x.nup, ndn=x.ndn, tii=x.tii, pii=x.pii, pic=x.pic) for x in DBInterface.execute(conn, """
        SELECT schedule.idx AS idx, nup, ndn, tii, pii, pic
        FROM sectors
        INNER JOIN schedule ON sectors.idx = schedule.idx
        WHERE schedule.type = 'small'
    """)]
    DBInterface.close!(conn)

    return process_dense(shape, t, U, temperatures, sectors; commitevery=commitevery)
end


function process_dense(
    shape::AbstractMatrix{<:Integer},
    t::Real,
    U::Real,
    temperatures::AbstractVector{<:Real},
    sector_indices::AbstractVector{<:Integer},
    ;
    force::Bool=false,
    commitevery::Integer=1,
)
    shape_str = shape_string(shape)
    sectors_filepath = datadir("square", "curie", shape_str, "sectors.sqlite3")
    conn = initialize_database(sectors_filepath)
    all_sectors = [(idx=x.idx, nup=x.nup, ndn=x.ndn, tii=x.tii, pii=x.pii, pic=x.pic) for x in DBInterface.execute(conn, """
        SELECT sectors.idx AS idx, nup, ndn, tii, pii, pic
        FROM sectors
        INNER JOIN schedule ON sectors.idx = schedule.idx
    """)]
    sectors = [x for x in all_sectors if x.idx in sector_indices]
    DBInterface.close!(conn)

    return process_dense(shape, t, U, temperatures, sectors; commitevery=commitevery, force=force)
end


function process_dense(
    shape::AbstractMatrix{<:Integer},
    t::Real,
    U::Real,
    temperatures::AbstractVector{<:Real},
    sectors::AbstractVector{SectorType},
    ;
    force::Bool=false,
    commitevery::Integer=1,
)
    BR = UInt

    # Set up lattice
    square = make_square_lattice(shape)

    lattice = square.lattice
    ssymbed = square.space_symmetry_embedding
    tsymbed = ssymbed.normal
    psymbed = ssymbed.rest

    hs, hamiltonian, c, cdag = make_hamiltonian_ladder(square, t, U)
    @mylogmsg "Finished making Hamiltonian"

    pair_groups = Vector{Tuple{Int, Int}}[]
    let visited = falses(numsites(lattice.supercell), numsites(lattice.supercell))
        for (i1, (sitename1, sitefc1)) in enumerate(lattice.supercell.sites),
            (i2, (sitename2, sitefc2)) in enumerate(lattice.supercell.sites)
            visited[i1, i2] && continue
            pair_group = Set()
            for t in tsymbed.elements, p in psymbed.elements
                j1 = p(t(i1))
                j2 = p(t(i2))
                push!(pair_group, (j1, j2))
                visited[j1, j2] = true
            end
            push!(pair_groups, sort(collect(pair_group)))
        end
    end

    nup(i::Integer) = cdag(i, :up) * c(i, :up)
    ndn(i::Integer) = cdag(i, :dn) * c(i, :dn)

    correlation = []

    @info "pair groups: $(pair_groups)"

    for pair_group in pair_groups
        push!(correlation, sum(nup(i) * nup(j) for (i, j) in pair_group) / length(pair_group))
        push!(correlation, sum(nup(i) * ndn(j) for (i, j) in pair_group) / length(pair_group))
        push!(correlation, sum(ndn(i) * nup(j) for (i, j) in pair_group) / length(pair_group))
        push!(correlation, sum(ndn(i) * ndn(j) for (i, j) in pair_group) / length(pair_group))
    end

    @assert all(isinvariant(x, y) for x in tsymbed.elements for y in correlation)
    @assert all(isinvariant(x, y) for x in psymbed.elements for y in correlation)

    correlation = [make_projector_operator(op, BR) for op in correlation]

    prev_idx = nothing
    rhsr = nothing

    prev_nupndn = nothing
    hsr = nothing

    prev_tii = nothing
    tsa = nothing
    psymbed_little = nothing

    prev_tiipiipic = nothing
    psa = nothing
    ssa = nothing

    @mylogmsg "Opening DB file"

    shape_str = shape_string(shape)
    sectors_filepath = datadir("square", "curie", shape_str, "sectors.sqlite3")
    temp_filepath = datadir("square", "curie", shape_str, "temp-dense-$(uuid5(uuid1(), gethostname())).sqlite3")
    conn = initialize_database(sectors_filepath, temp_filepath)
    DBInterface.execute(conn, "BEGIN")

    commitcount = 0
    for (idx, nup, ndn, tii, pii, pic) in sectors
        @mylogmsg "Sector: idx=$idx, nup=$nup, nn=$ndn, tii=$tii, pii=$pii, pic=$pic"

        existing_temperatures = [
            x.temperature for x in DBInterface.execute(conn, """
                    SELECT temperature
                    FROM dense_results
                    WHERE idx=? AND hopping=? AND interaction=?
                """,
                (idx, t, U)
            )
        ]

        if existing_temperatures == temperatures && !force
            @mylogmsg "Empty temperature list. Skipping $idx, $nup, $ndn, $tii, $pii, $pic."
            continue
        end
        select_temperatures = temperatures

        if idx == prev_idx
            @mylogmsg "Using previous sector rhsr"
            @assert !isnothing(rhsr)
        else
            if (nup, ndn) == prev_nupndn
                @mylogmsg "Using previous hsr"
                @assert !isnothing(hsr)
            else
                hsr = represent(hs, [(nup, ndn),], BR, DictIndexedVector)
                prev_nupndn = (nup, ndn)
                @mylogmsg "Finished creating hsr"
            end
            if tii == prev_tii
                @mylogmsg "Using previous tii"
                @assert !isnothing(tsa)
                @assert !isnothing(psymbed_little)
            else
                tsa = collect(get_irrep_iterator(IrrepComponent(tsymbed, tii)))
                psymbed_little = little_symmetry(tsymbed, tii, psymbed)
                prev_tii = tii
            end
            if (tii, pii, pic) == prev_tiipiipic
                @mylogmsg "Using previous psa, ssa"
                @assert !isnothing(psa)
                @assert !isnothing(ssa)
            else
                psa = collect(get_irrep_iterator(IrrepComponent(psymbed_little, pii, pic)))
                ssa = make_product_irrep(psa, tsa)
                prev_tiipiipic = (tii, pii, pic)
            end
            rhsr = symmetry_reduce(hsr, ssa)
            @mylogmsg "Finished creating rhsr"
        end
        D = dimension(rhsr)
        if D == 0
            @mylogmsg "Sector has zero dimension. Skip."
            continue
        end

        @mylogmsg "Generating Hamiltonian representation"
        h = represent(rhsr, hamiltonian)

        @mylogmsg "Generating observables representation"
        observables = [Observable(represent(rhsr, ss)) for ss in correlation]

        @mylogmsg "Start computing"
        ret = compute_thermodynamics_dense(h, select_temperatures, observables)
        @mylogmsg "Finished computing"

        base_energy = ret.base_energy
        idx_list = [idx for (iT, T) in enumerate(select_temperatures)]
        t_list   = [t for (iT, T) in enumerate(select_temperatures)]
        U_list   = [U for (iT, T) in enumerate(select_temperatures)]
        T_list   = [T for (iT, T) in enumerate(select_temperatures)]
        z_list   = [ret.z[iT] for (iT, T) in enumerate(select_temperatures)]
        E_list   = [ret.E[iT] for (iT, T) in enumerate(select_temperatures)]
        E2_list  = [ret.E2[iT] for (iT, T) in enumerate(select_temperatures)]
        obs_list = [
            JSON.json([real(ret.obs[iobs][iT]) for iobs in 1:length(observables)])
            for (iT, T) in enumerate(select_temperatures)
        ]

        @mylogmsg "Start writing"
        DBInterface.executemany(conn, """
                INSERT OR REPLACE INTO temporary.dense_energy_shifts
                (idx, hopping, interaction, base_energy)
                VALUES (?, ?, ?, ?)
            """, (idx, t, U, base_energy)
        )
        DBInterface.executemany(conn, """
                INSERT OR REPLACE INTO temporary.dense_results
                (idx, hopping, interaction, temperature, partition, energy, energy_squared, correlation)
                VALUES (?, ?, ?, ?, ?, ?, ?, ?)
            """, (idx_list, t_list, U_list, T_list, z_list, E_list, E2_list, obs_list))

        @mylogmsg "Finished writing"
        commitcount += 1

        if commitcount >= commitevery
            DBInterface.execute(conn, "COMMIT")
            DBInterface.execute(conn, "BEGIN")
            commitcount = 0
        end
    end
    DBInterface.execute(conn, "COMMIT")

    DBInterface.close!(conn)
end


function parse_commandline()
    s = ArgParse.ArgParseSettings()
    @add_arg_table! s begin
        "shape"
            arg_type = String
            help = "shape of the lattice in the format (?,?)x(?,?)"
            required = true
        "--hopping", "-t"
            arg_type = Float64
            default = 1.0
        "--interaction", "-U"
            arg_type = Float64
            required = true
        "--temperature", "-T"
            arg_type = Float64
            nargs = '+'
            required = true
        "--sector"
            arg_type = Int
            nargs = '*'
        "--force", "-f"
            action = :store_true
        "--debug", "-d"
            action = :store_true
        "--commitevery"
            arg_type = Int
            default = 1
    end
    parse_args(s)
end


function main()
    parsed_args = parse_commandline()
    if parsed_args["debug"]
        logger = ConsoleLogger(stdout, Logging.Debug; meta_formatter=my_metafmt)
        global_logger(logger)
    else
        logger = ConsoleLogger(stdout, Logging.Info; meta_formatter=my_metafmt)
        global_logger(logger)
    end

    shape = parse_shape(parsed_args["shape"])
    t = parsed_args["hopping"]
    U = parsed_args["interaction"]
    temperatures = parsed_args["temperature"]

    sectors = parsed_args["sector"]
    commitevery = parsed_args["commitevery"]
    force = parsed_args["force"]

    @mylogmsg "Starting $(first(ARGS))"
    @mylogmsg "shape: $shape"
    @mylogmsg "t: $t"
    @mylogmsg "U: $U"
    @mylogmsg "T: $temperatures"
    @mylogmsg "sectors: $sectors"
    @mylogmsg "BLAS: $(BLAS.get_config())"
    @mylogmsg "commitevery: $commitevery"
    @mylogmsg "force: $force"

    if isempty(sectors)
        process_small(shape, t, U, temperatures; commitevery=commitevery)
    else
        process_dense(shape, t, U, temperatures, sectors; commitevery=commitevery, force=force)
    end
    @mylogmsg "Finished writing"
end


main()

