using DrWatson
@quickactivate "TriangularHubbard"

using TriangularHubbard

using LinearAlgebra
using Random
using LatticeTools
using QuantumHamiltonian
using QuantumHamiltonianParticle

using Formatting
using Logging
using ArgParse

using SQLite
using DBInterface
using ProgressMeter



function compute_sectors(shape::AbstractMatrix{<:Integer}, nup::Integer, ndn::Integer)
    shape_str = shape_string(shape)
    if isfile(datadir("sectors-$shape_str-($nup,$ndn).csv"))
        return
    end

    # Set up lattice
    triangular = make_triangular_lattice(shape)
    n_sites = numsites(triangular.lattice.supercell)

    lattice = triangular.lattice
    ssymbed = triangular.space_symmetry_embedding
    tsymbed = ssymbed.normal
    psymbed = ssymbed.rest

    # Set up particle sector and Hilbert space
    ps, c, cdag = electron_system()

    em = ParticleState(ps, "em", [0, 0], (0, 0))
    up = ParticleState(ps, "up", [1, 0], (1, 0))
    dn = ParticleState(ps, "dn", [0, 1], (0, 1))
    ud = ParticleState(ps, "ud", [1, 1], (1, 1))
    site = ParticleSite([em, up, dn, ud])

    hs = ParticleHilbertSpace([site for i in 1:n_sites])

    ssa_collection = []
    for tii in 1:irrepcount(tsymbed)
        tsa = collect(get_irrep_iterator(IrrepComponent(tsymbed, tii)))
        psymbed_little = little_symmetry(tsymbed, tii, psymbed)
        for pii in 1:num_irreps(psymbed_little), pic in 1:irrep_dimension(psymbed_little, pii)
            psa = get_irrep_iterator(IrrepComponent(psymbed_little, pii, pic))
            ssa = collect(make_product_irrep(psa, tsa))
            push!(ssa_collection, ((tii, pii, pic), ssa))
        end
    end

    (tmppath, tmpio) = mktemp(cleanup=false)

    #open(datadir("sectors-$shape_str-($nup,$ndn).csv"), "w") do io
    println(tmpio, "nup,ndn,tii,pii,pic,dim")
    #end
    let qn = (nup, ndn)
        hsr = represent(HilbertSpaceSector(hs, qn))
        for ((tii, pii, pic), ssa) in ssa_collection
            println("sector ($tii,$pii,$pic)")
            rhsr = symmetry_reduce(hsr, ssa)
            dim = dimension(rhsr)
            #open(datadir("sectors-$shape_str-($nup,$ndn).csv"), "a") do io
                #println(io, nup,",",ndn,",",tii,",",pii,",",pic,",",dim)
            println(tmpio, nup,",",ndn,",",tii,",",pii,",",pic,",",dim)
            #end
        end
    end # for qn
    close(tmpio)
    cp(tmppath, datadir("sectors-$shape_str-($nup,$ndn).csv"))
end # function compute


function parse_commandline()
    s = ArgParse.ArgParseSettings()
    @add_arg_table! s begin
        "shape"
            arg_type = String
            help = "shape of the lattice in the format (?,?)x(?,?)"
            required = true
        "--nup"
            arg_type = Int
            required = true
        "--ndn"
            arg_type = Int
            required = true
        "--debug", "-d"
            action = :store_true
    end
    parse_args(s)
end


function main()
    parsed_args = parse_commandline()

    parsed_args = parse_commandline()
    if parsed_args["debug"]
        logger = ConsoleLogger(stdout, Logging.Debug; meta_formatter=my_metafmt)
        global_logger(logger)
    else
        logger = ConsoleLogger(stdout, Logging.Info; meta_formatter=my_metafmt)
        global_logger(logger)
    end

    shape = parse_shape(parsed_args["shape"])
    nup = parsed_args["nup"]
    ndn = parsed_args["ndn"]

    @mylogmsg "Computing sectors"
    @mylogmsg "shape: $shape"
    @mylogmsg "nup: $nup"
    @mylogmsg "ndn: $ndn"

    compute_sectors(shape, nup, ndn)
end


main()

