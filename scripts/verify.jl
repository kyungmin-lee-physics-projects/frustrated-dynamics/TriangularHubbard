using DrWatson
@quickactivate "TriangularHubbard"

using TriangularHubbard

using ArgParse
using CodecXz
using MsgPack
using DBInterface
using SQLite
using DataFrames

function parse_commandline()
    s = ArgParse.ArgParseSettings()
    @add_arg_table! s begin
        "shape"
            arg_type = String
            help = "shape of the lattice in the format (?,?)x(?,?)"
            required = true
    end
    return parse_args(s)
end


function main()
    parsed_args = parse_commandline()
    shape = parse_shape(parsed_args["shape"])
    shape_str = shape_string(shape)
    sector_filepath = datadir("curie", shape_str, "sectors.sqlite3")
    conn = DBInterface.connect(SQLite.DB, sector_filepath)

    # CHECK all sectors are scheduled
    r = DBInterface.execute(conn, """
        SELECT *
        FROM sectors
        LEFT JOIN schedule
        ON sectors.idx = schedule.idx
        WHERE schedule.type IS NULL AND dim > 0
    """)
    if !isempty(r)
        println("some sectors with nonzero dimension has no schedule")
        println(DataFrame(r))
    end

    # Check sparse files
    for (idx, filename) in DBInterface.execute(conn, "SELECT idx, filename FROM sparse_results")
        filepath = datadir("curie", shape_str, filename)
        if !isfile(filepath)
            println("Sparse file $filename does not exist (sector: $idx)")
            continue
        end
        try
            open(filepath, "r") do io
                ioc = XzDecompressorStream(io)
                data = MsgPack.unpack(ioc)
                close(ioc)
                close(io)

                if !haskey(data, "partition")
                    println("Partition missing (sector: $idx, filename: $filename)")
                elseif !haskey(data, "energy")
                    println("energy missing (sector: $idx, filename: $filename)")
                elseif !haskey(data, "energy_squared")
                    println("energy_squared missing (sector: $idx, filename: $filename)")
                else
                    n1 = length(data["partition"])
                    n2 = length(data["energy"])
                    n3 = length(data["energy_squared"])
                    if !(n1 == n2 == n3) 
                        println("lengths of z, E, E² do not match ($n1, $n2, $n3)")
                    end
                end
            end
        catch exc
            println("Error occured while reading $filename (sector: $idx)")
            println("Exception: $exc")
        end
    end

    # Check Missing data
    tUT_list = DataFrame(
        DBInterface.execute(conn, """
        SELECT DISTINCT hopping, interaction, temperature
        FROM dense_results
        UNION
        SELECT DISTINCT hopping, interaction, temperature
        FROM sparse_results
        """)
    )

    # check dense
    for (t, U, T) in eachrow(tUT_list)
        r = DBInterface.execute(conn, """
            SELECT schedule.idx, dim
            FROM schedule
            JOIN sectors ON schedule.idx=sectors.idx
            WHERE (type='dense' OR type='small') AND schedule.idx NOT IN (
                SELECT idx
                FROM dense_results
                WHERE hopping=? AND interaction=? AND temperature=?
            )
        """, (t, U, T))
        if !isempty(r)
            println("missing dense: t = ", t, ", U = ", U, ", T = ", T)
            println(DataFrame(r))
        end
    end

    # check sparse
    for (t, U, T) in eachrow(tUT_list)
        r = DBInterface.execute(conn, """
            SELECT schedule.idx, dim
            FROM schedule
            JOIN sectors ON schedule.idx=sectors.idx
            WHERE type='sparse' AND schedule.idx NOT IN (
                    SELECT idx
                    FROM sparse_results
                    WHERE hopping=? AND interaction=? AND temperature=?
                UNION
                    SELECT idx
                    FROM dense_results
                    WHERE hopping=? AND interaction=? AND temperature=?
            )
        """, (t, U, T, t, U, T))
        if !isempty(r)
            println("missing sparse: t = ", t, ", U = ", U, ", T = ", T)
            println(DataFrame(r))
        end
    end
    DBInterface.close!(conn)
end

main()
