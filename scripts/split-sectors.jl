using ArgParse
using DataFrames
using CSV
using JSON
using DataStructures
using DBInterface
using SQLite


function parse_commandline()
    s = ArgParse.ArgParseSettings()
    @add_arg_table! s begin
        "inputfilename"
            arg_type = String
            required = true
        "--out", "-o"
            arg_type = String
    end
    parse_args(s)
end


function main()
    parsed_args = parse_commandline()
    #df = CSV.read(parsed_args["inputfilename"], DataFrame)
    df = let
        conn = DBInterface.connect(SQLite.DB, parsed_args["inputfilename"])
        DBInterface.execute(conn, "SELECT * FROM sectors") |> DataFrame
    end
    idx_empty  =          df[:, :dim] .<= 0
    idx_small  =     0 .< df[:, :dim] .<= 1000
    idx_medium =  1000 .< df[:, :dim] .<= 20000
    idx_large  = 20000 .< df[:, :dim]

    if isnothing(parsed_args["out"])
        outfp = stdout
    else
        outfp = open(parsed_args["out"], "w")
    end

    println(outfp, JSON.json(OrderedDict(
        "empty" => df[idx_empty, :idx],
        "small" => df[idx_small, :idx],
        "medium"=> df[idx_medium, :idx],
        "large" => df[idx_large, :idx],
    )))
    flush(outfp)
    if outfp != stdout
        close(outfp)
    end

    # @show idx_small .& idx_medium .& idx_large
    # @show unique(idx_small .+ idx_medium .+ idx_large)
end


main()
