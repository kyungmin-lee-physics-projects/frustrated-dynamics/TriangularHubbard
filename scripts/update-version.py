"""
Update sparse_results
Add column `seed` to the sparse_results.
"""
import sqlite3
import argparse

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("filename", type=str, nargs='+')
    args = parser.parse_args()
    for filename in args.filename:
        print("Processing {}".format(filename))
        con = sqlite3.connect(filename)
        cur = con.cursor()

        cur.execute("""PRAGMA journal_mode=DELETE;""")
        cur.execute("""
            SELECT sql
            FROM sqlite_master
            WHERE type='table' AND name='sparse_results' AND tbl_name='sparse_results';
        """);
        row = cur.fetchone()
        if not row:
            print("sparse_results missing")
            # create table
            cur.execute("""
                CREATE TABLE sparse_results (
                    idx INTEGER NOT NULL,
                    hopping REAL NOT NULL,
                    interaction REAL NOT NULL,
                    temperature REAL NOT NULL,
                    seed INTEGER NOT NULL,
                    nsamples INTEGER NOT NULL,
                    filename TEXT NOT NULL,
                    UNIQUE(idx, hopping, interaction, temperature, seed)
                );
            """)
        elif "samplecount" not in row[0]:
            print("sparse_results missing seed")
            cur.execute("BEGIN;")
            cur.execute("ALTER TABLE sparse_results RENAME TO sparse_results_old;")
            cur.execute("""
                CREATE TABLE sparse_results (
                    idx INTEGER NOT NULL,
                    hopping REAL NOT NULL,
                    interaction REAL NOT NULL,
                    temperature REAL NOT NULL,
                    seed INTEGER NOT NULL,
                    nsamples INTEGER NOT NULL,
                    filename TEXT NOT NULL,
                    UNIQUE(idx, hopping, interaction, temperature, seed)
                );
            """)
            cur.execute("""
                INSERT INTO sparse_results
                    SELECT idx, hopping, interaction, temperature, 0, 1000, filename
                    FROM sparse_results_old
                ;
            """)
            cur.execute("COMMIT;")
        else:
            print("sparse_results already up to date")

        con.close()

main()
