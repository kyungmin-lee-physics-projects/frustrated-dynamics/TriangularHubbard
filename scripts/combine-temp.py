#!/usr/bin/env python3
import glob
import os, sys
import sqlite3
import argparse

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-o", type=str, default='sectors.sqlite3')
    parser.add_argument("input", type=str, nargs='+')
    args = parser.parse_args()

    con = sqlite3.connect(args.o)
    cur = con.cursor()

    for fname in args.input:
        print(f"processing {fname}")
        cur.execute('''ATTACH DATABASE ? AS temporary''', (fname,))
        cur.execute('''BEGIN''')
        cur.execute('''INSERT OR REPLACE INTO dense_energy_shifts SELECT * FROM temporary.dense_energy_shifts''')
        cur.execute('''DELETE FROM temporary.dense_energy_shifts''')
        cur.execute('''INSERT OR REPLACE INTO dense_results SELECT * FROM temporary.dense_results''')
        cur.execute('''DELETE FROM temporary.dense_results''')
        cur.execute('''COMMIT''')
        cur.execute('''VACUUM temporary''')
        cur.execute('''DETACH DATABASE temporary''')
        os.rename(fname, "{}.processed".format(fname))
    con.close()

if __name__=='__main__':
    main()
