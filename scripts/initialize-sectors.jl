using DrWatson
@quickactivate "TriangularHubbard"

using TriangularHubbard

using LinearAlgebra
using Random
using LatticeTools
using QuantumHamiltonian
using QuantumHamiltonianParticle

using Formatting
using Logging
using ArgParse

using SQLite
using DBInterface
using ProgressMeter

function initialize_database(sector_filepath::AbstractString)
    conn = DBInterface.connect(SQLite.DB, sector_filepath)
    results = DBInterface.execute(conn, "SELECT name FROM sqlite_master WHERE type=? AND name=?", ("table", "sectors"))
    if isempty(results)
        DBInterface.execute(conn, """
        CREATE TABLE sectors (
            idx INTEGER NOT NULL PRIMARY KEY,
            nup INTEGER NOT NULL,
            ndn INTEGER NOT NULL,
            tii INTEGER NOT NULL,
            pii INTEGER NOT NULL,
            pic INTEGER NOT NULL,
            dim INTEGER NOT NULL,
            UNIQUE(nup, ndn, tii, pii, pic)
        )""")
    end
    return conn
end


function initialize_sectors(shape::AbstractMatrix{<:Integer})
    shape_str = shape_string(shape)

    # Set up lattice
    triangular = make_triangular_lattice(shape)
    n_sites = numsites(triangular.lattice.supercell)

    lattice = triangular.lattice
    ssymbed = triangular.space_symmetry_embedding
    tsymbed = ssymbed.normal
    psymbed = ssymbed.rest

    # Set up particle sector and Hilbert space
    ps, c, cdag = electron_system()

    em = ParticleState(ps, "em", [0, 0], (0, 0))
    up = ParticleState(ps, "up", [1, 0], (1, 0))
    dn = ParticleState(ps, "dn", [0, 1], (0, 1))
    ud = ParticleState(ps, "ud", [1, 1], (1, 1))
    site = ParticleSite([em, up, dn, ud])

    hs = ParticleHilbertSpace([site for i in 1:n_sites])

    irreps = []
    for tii in 1:irrepcount(tsymbed)
        psymbed_little = little_symmetry(tsymbed, tii, psymbed)
        for pii in 1:num_irreps(psymbed_little), pic in 1:irrep_dimension(psymbed_little, pii)
            push!(irreps, (tii, pii, pic))
        end
    end

    if !isdir(datadir())
        mkpath(datadir())
    end
    conn = initialize_database(datadir("sectors-$shape_str.sqlite3"))

    idx = 0
    total_dimension = 0
    DBInterface.execute(conn, "BEGIN")
    for qn in quantum_number_sectors(hs)
        nup, ndn = qn
        for (tii, pii, pic) in irreps
            idx += 1
            DBInterface.execute(conn, "INSERT OR IGNORE INTO sectors VALUES (?, ?, ?, ?, ?, ?, ?)", (idx, nup, ndn, tii, pii, pic, -1))
        end
    end
    DBInterface.execute(conn, "COMMIT")
    DBInterface.close!(conn)
end # function compute


function parse_commandline()
    s = ArgParse.ArgParseSettings()
    @add_arg_table! s begin
        "shape"
            arg_type = String
            help = "shape of the lattice in the format (?,?)x(?,?)"
            required = true
    end
    parse_args(s)
end


function main()
    parsed_args = parse_commandline()
    shape = parse_shape(parsed_args["shape"])
    initialize_sectors(shape)
end


main()

