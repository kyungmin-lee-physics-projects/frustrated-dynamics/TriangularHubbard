using DrWatson
@quickactivate "TriangularHubbard"

using TriangularHubbard

using ArgParse

using SQLite
using DBInterface


"""
    extract(shape, type)

extract type from schedule
"""
function extract(
    shape::AbstractMatrix{<:Integer},
    type::String
)
    shape_str = shape_string(shape)
    sectors_filepath = datadir("curie", shape_str, "sectors.sqlite3")
    conn = DBInterface.connect(SQLite.DB, sectors_filepath)
    sectors = [(idx=x.idx, nup=x.nup, ndn=x.ndn, tii=x.tii, pii=x.pii, pic=x.pic) for x in DBInterface.execute(conn, """
        SELECT schedule.idx AS idx, nup, ndn, tii, pii, pic
        FROM sectors
        INNER JOIN schedule ON sectors.idx = schedule.idx
        WHERE schedule.type = ?
    """, (type, ))]
    return sectors
end


function parse_commandline()
    s = ArgParse.ArgParseSettings(
        description="extract sectors of particular type from schedule",
    )
    @add_arg_table! s begin
        "shape"
            arg_type = String
            help = "shape of the lattice in the format (?,?)x(?,?)"
            required = true
        "type"
            arg_type = String
            required = true
            help = "type of matrix. one of small, dense, or sparse"
    end
    parse_args(s)
end

#=
    Usage: julia .\extract.jl "(3,0)x(0,3)" "dense"
=#
function main()
    parsed_args = parse_commandline()
    shape = parse_shape(parsed_args["shape"])
    sectors = extract(shape, parsed_args["type"])
    for s in sectors
        println(s.idx)
    end
end


main()

