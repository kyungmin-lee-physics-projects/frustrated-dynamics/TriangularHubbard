using DrWatson
@quickactivate "TriangularHubbard"

using LinearAlgebra

using LatticeTools
using KrylovKit
using QuantumHamiltonian
using QuantumHamiltonianParticle
using Plots

function main()
    n_sites = 4
    t = 1.0
    U = 4.0

    unitcell = makeunitcell(1.0; SiteType=String)
    addsite!(unitcell, "A", FractCoord([0,], [0.0]))
    lattice = makelattice(unitcell, hcat(n_sites))

    tsym = FiniteTranslationSymmetry(lattice)
    psym = little_symmetry(tsym, project(PointSymmetryDatabase.find("-1"), [1 0 0;]))

    tsymbed = embed(lattice, tsym)
    psymbed = embed(lattice, psym)

    ps = ParticleSector(Fermion("↑"), Fermion("↓"))
    cup(i::Integer) = ParticleLadderUnit(ps, 1, i, ANNIHILATION)
    cupdag(i::Integer) = ParticleLadderUnit(ps, 1, i, CREATION)
    cdn(i::Integer) = ParticleLadderUnit(ps, 2, i, ANNIHILATION)
    cdndag(i::Integer) = ParticleLadderUnit(ps, 2, i, CREATION)


    em = ParticleState(ps, "em", [0, 0], (0,0))
    up = ParticleState(ps, "up", [1, 0], (1,1))
    dn = ParticleState(ps, "dn", [0, 1], (1,-1))
    ud = ParticleState(ps, "ud", [1, 1], (2,0))
    
    site = ParticleSite([em, up, dn, ud])

    hs = ParticleHilbertSpace([site for i in 1:n_sites])
    hopping = ParticleLadderNull(ps)

    for i in 1:n_sites
        j = mod(i, n_sites) + 1
        hopping += -t * (cupdag(j) * cup(i) + cupdag(i) * cup(j))
        hopping += -t * (cdndag(j) * cdn(i) + cdndag(i) * cdn(j))
    end
    # @show hopping

    interaction = ParticleLadderNull(ps)
    for i in 1:n_sites
        interaction += U * cupdag(i) * cup(i) * cdndag(i) * cdn(i)
    end

    hamiltonian = embed(hs, hopping + interaction)

    all_eigenvalues_symred = Float64[]
    totaldim = 0
    for qn in quantum_number_sectors(hs)
        hss = HilbertSpaceSector(hs, qn)
        hsr = represent(hss)

        for tii in 1:irrepcount(tsymbed)
            psymbed_little = little_symmetry(tsymbed, tii, psymbed)
            tsa = collect(get_irrep_iterator(IrrepComponent(tsymbed, tii)))
            for pii in 1:num_irreps(psymbed_little), pic in 1:irrep_dimension(psymbed_little, pii)
                psa = collect(get_irrep_iterator(IrrepComponent(psymbed_little, pii, pic)))
                ssa = make_product_irrep(psa, tsa)
                rhsr = symmetry_reduce(hsr, ssa)
                h = represent(rhsr, hamiltonian)
                if size(h, 1) == 0
                    continue
                end
                totaldim += size(h, 1)
                H = Matrix(h)
                append!(all_eigenvalues_symred, eigvals(Hermitian(H)))
            end
        end
    end
    @show totaldim
    sort!(all_eigenvalues_symred)
    H = represent(represent(hs), hamiltonian)
    all_eigenvalues = eigvals(Hermitian(H))
    sort!(all_eigenvalues)
    @show all_eigenvalues_symred - all_eigenvalues

    plot()
    plot!(all_eigenvalues)
    plot!(all_eigenvalues_symred)
end

main()


