using DrWatson
@quickactivate "TriangularHubbard"

using TriangularHubbard

using LinearAlgebra
using Random

using LatticeTools
using KrylovKit
using QuantumHamiltonian
using QuantumHamiltonianParticle
using FiniteTemperatureLanczos

using DataStructures
using CodecXz
using MsgPack
using Formatting

using Logging
using ArgParse
using ProgressMeter

function compute_stuff(
    H::AbstractMatrix{<:Number},
    temperatures::AbstractVector{<:AbstractFloat},
    observables::AbstractVector{<:AbstractObservable},
    rng::AbstractRNG,
    matrix_type::AbstractString,
    ;
    # maxdense::Integer=20000,
    krylovdim::Integer=200,
    nsamples::Integer=1000,
)
    D = size(H, 1)
    if matrix_type == "dense"
        @mylogmsg "  matrix: $D (dense)"
        eigenvalues, eigenvectors = eigen(Hermitian(Matrix(H)))
        @mylogmsg "  diagonalized"
        z, E, E2 = Float64[], Float64[], Float64[]
        for T in temperatures
            boltzmann = exp.(-eigenvalues ./ T)
            push!(z, sum(boltzmann))
            push!(E, sum(eigenvalues .* boltzmann))
            push!(E2, sum(eigenvalues.^2 .* boltzmann))
        end
        obs_list = []

        function manybodylindhard(T::Real, tol::Real=Base.rtoldefault(Float64))
            boltzmann = exp.(-eigenvalues ./ T)
            function (i1::Integer, i2::Integer)
                e1, e2 = eigenvalues[i1], eigenvalues[i2]
                b1, b2 = boltzmann[i1], boltzmann[i2]
                return (abs(e1-e2) < tol) ? (0.5 * (b1+b2) / T) : ((b1 - b2) / (e2 - e1))
            end
        end

        for obs in observables
            if isa(obs, Observable)
                A = adjoint(eigenvectors) * obs.observable * eigenvectors
                push!(obs_list, 
                    [sum( diag(A) .* exp.(-eigenvalues ./ T)) for T in temperatures]
                )
            elseif isa(obs, Susceptibility)
                A = adjoint(eigenvectors) * obs.observable * eigenvectors
                B = adjoint(eigenvectors) * obs.field * eigenvectors

                push!(obs_list,
                    [
                        let mblh = manybodylindhard(t)
                            sum(
                                A[a,b] * B[b,a] * mblh(a, b)
                                    for a in 1:length(eigenvalues)
                                    for b in 1:length(eigenvalues)
                            )
                        end
                            for t in temperatures
                    ]
                )
            else
                throw(ArgumentError("unsupported observable $obs"))
            end
        end
        @mylogmsg "  computed observables"
        return (matrix_type=:dense, z=z, E=E, E2=E2, obs=obs_list)
    elseif matrix_type == "sparse"
        krylovdim = min(D, krylovdim)
        @mylogmsg "  matrix: $D (sparse / $krylovdim)"

        zs = [zeros(Float64, nsamples) for T in temperatures]
        Es = [zeros(Float64, nsamples) for T in temperatures]
        E2s = [zeros(Float64, nsamples) for T in temperatures]
        obss_list = [[zeros(eltype(obs), nsamples) for T in temperatures] for obs in observables]

        @showprogress for r in 1:nsamples
            v = rand(rng, ComplexF64, D) * 2 .- (1 + im)
            normalize!(v)
        
            iterator = LanczosIterator(x -> H*x, v)
            factorization = initialize(iterator)
            for _ in 1:krylovdim
                expand!(iterator, factorization)
            end
            pm = premeasure(factorization)
            pre_z  = premeasure(pm, 0)
            pre_E  = premeasure(pm, 1)
            pre_E2 = premeasure(pm, 2)
            pre_obs_list = [premeasure(pm, obs) for obs in obervables]

            for (iT, T) in enumerate(temperatures)
                zs[iT][r]  = measure(pre_z,  pm.eigen.values, T)
                Es[iT][r]  = measure(pre_E,  pm.eigen.values, T)
                E2s[iT][r] = measure(pre_E2, pm.eigen.values, T)
                for (iobs, pre_obs) in enumerate(pre_obs_list)
                    obss_list[iobs][iT][r] = measure(pre_obs, pm.eigen.values, T)
                end
            end
        end # for r
        @mylogmsg "  collected $nsamples samples"
        return (matrix_type=:sparse, z=zs, E=Es, E2=E2s, obs=obss_list)
    else   
        error("Unsupported matrix type $matrix_type")
    end # if dnese/sparse
end

function compute(
    shape::AbstractMatrix{<:Integer},
    t::Real,
    U::Real,
    temperatures::AbstractVector{<:Real},

    n_list::AbstractVector{<:Integer},
    Sz_list::AbstractVector{<:Real},
    tii_list::AbstractVector{<:Integer},
    pii_list::AbstractVector{<:Integer},
    pic_list::AbstractVector{<:Integer},
    ;
    maxdense::Integer=20000,
    krylovdim::Integer=200,
    nsamples::Integer=1000,
    seed::Integer=0,
    force::Bool=false
)
    rng = MersenneTwister(seed)

    shape_str = shape_string(shape)

    # Set up lattice
    triangular = make_triangular_lattice(shape)
    n_sites = numsites(triangular.lattice.supercell)

    lattice = triangular.lattice
    ssymbed = triangular.space_symmetry_embedding
    tsymbed = ssymbed.normal
    psymbed = ssymbed.rest

    # Set up particle sector and Hilbert space
    ps, c, cdag = electron_system()

    em = ParticleState(ps, "em", [0, 0], (0, 0))
    up = ParticleState(ps, "up", [1, 0], (1, 1))
    dn = ParticleState(ps, "dn", [0, 1], (1,-1))
    ud = ParticleState(ps, "ud", [1, 1], (2, 0))
    site = ParticleSite([em, up, dn, ud])

    hs = ParticleHilbertSpace([site for i in 1:n_sites])

    # Construct operators (Hamiltonian and measurement)
    hopping = ParticleLadderNull(ps)
    hopping = sum(
        cdag(j, σ) * c(i, σ) + cdag(i, σ) * c(j, σ)
            for ((i, j), R) in triangular.nearest_neighbor_bonds
            for σ in [:up, :dn]
    ) * (-t)
    interaction = sum(
        cdag(i, :up) * c(i, :up) * cdag(i, :dn) * c(i, :dn)
            for i in 1:n_sites
    ) * U
    hamiltonian = embed(hs, hopping + interaction)

    for qn in quantum_number_sectors(hs)
        charge, two_spin = qn
        Sz = two_spin * 0.5

        (!isempty(n_list) && (charge ∉ n_list)) && continue
        (!isempty(Sz_list) && (Sz ∉ Sz_list)) && continue

        hsr = represent(HilbertSpaceSector(hs, qn))
        
        for tii in 1:irrepcount(tsymbed)
            (!isempty(tii_list) && (tii ∉ tii_list)) && continue
            tsa = collect(get_irrep_iterator(IrrepComponent(tsymbed, tii)))

            psymbed_little = little_symmetry(tsymbed, tii, psymbed)
            for pii in 1:num_irreps(psymbed_little), pic in 1:irrep_dimension(psymbed_little, pii)
                (!isempty(pii_list) && (pii ∉ pii_list)) && continue
                (!isempty(pic_list) && (pic ∉ pic_list)) && continue

                # Preselect parameters. If dense result exists, don't run unless forced.
                preselect_temperatures = Float64[]
                for (iT, T) in enumerate(temperatures)
                    parameter_directory = Dict{Symbol, Any}(
                        :t => format("{:.3f}", t),
                        :U => format("{:.3f}", U),
                        :T => format("{:.3f}", T),
                    )
                    parameter_filename = Dict{Symbol, Any}(
                        :n => charge,
                        :Sz => Sz,
                        :tii => tii,
                        :pii => pii,
                        :pic => pic,
                        :type => "dense",
                    )
                    output_filename = savename("ftlm", parameter_filename, "msgpack.xz")
                    output_filepath = datadir("curie", shape_str, savename(parameter_directory), output_filename)

                    if ispath(output_filepath)
                        if force
                            @mylogmsg "File $output_filepath exists. Overwriting."
                            push!(preselect_temperatures, T)
                        else
                            @mylogmsg "File $output_filepath exists. Skipping."
                        end
                    else
                        push!(preselect_temperatures, T)
                    end
                end # for T

                psa = collect(get_irrep_iterator(IrrepComponent(psymbed_little, pii, pic)))
                ssa = make_product_irrep(psa, tsa)
                rhsr = symmetry_reduce(hsr, ssa)
                dimension(rhsr) == 0 && continue

                # --------------------------------------------------------------
                matrix_type = (dimension(rhsr) <= maxdense) ? "dense" : "sparse"

                h = represent(rhsr, hamiltonian)

                select_temperatures = Float64[]
                for (iT, T) in enumerate(preselect_temperatures)
                    parameter_directory = Dict{Symbol, Any}(
                        :t => format("{:.3f}", t),
                        :U => format("{:.3f}", U),
                        :T => format("{:.3f}", T),
                    )
                    parameter_filename = Dict{Symbol, Any}(
                        :n => charge,
                        :Sz => Sz,
                        :tii => tii,
                        :pii => pii,
                        :pic => pic,
                        :type => matrix_type,
                    )
                    output_filename = savename("ftlm", parameter_filename, "msgpack.xz")
                    output_filepath = datadir("curie", shape_str, savename(parameter_directory), output_filename)

                    if ispath(output_filepath)
                        if force
                            @mylogmsg "File $output_filepath exists. Overwriting."
                            push!(select_temperatures, T)
                        else
                            @mylogmsg "File $output_filepath exists. Skipping."
                        end
                    else
                        push!(select_temperatures, T)
                    end
                end # for temperature

                isempty(select_temperatures) && continue

                @mylogmsg "Computing sector: $qn, $tii, $pii, $pic"

                result = compute_stuff(
                    h, select_temperatures, Observable[], rng, matrix_type;
                    krylovdim=krylovdim, nsamples=nsamples
                )

                for (iT, T) in enumerate(select_temperatures)
                    save_data = OrderedDict(
                        "parameter" => OrderedDict(
                            "shape" => [shape[1,1], shape[2,1], shape[1,2], shape[2,2]],
                            "hopping" => t,
                            "interaction" => U,
                            "temperature" => T,
                            "t" => t,
                            "U" => U,
                            "T" => T,
                        ),
                        "sector" => OrderedDict(
                            "n" => charge,
                            "Sz" => Sz,
                            "tii" => tii,
                            "pii" => pii,
                            "pic" => pic,
                            "dimension" => dimension(rhsr),
                            "matrix_type" => matrix_type,
                        ),
                        "measurement" => OrderedDict(
                            "partition" => result.z[iT],
                            "energy" => result.E[iT],
                            "energy_squared" => result.E2[iT],
                        ),
                    )
                    parameter_directory = Dict{Symbol, Any}(
                        :t => format("{:.3f}", t),
                        :U => format("{:.3f}", U),
                        :T => format("{:.3f}", T),
                    )
                    parameter_filename = Dict{Symbol, Any}(
                        :n => charge,
                        :Sz => Sz,
                        :tii => tii,
                        :pii => pii,
                        :pic => pic,
                        :type => matrix_type,
                    )
                    output_filename = savename("ftlm", parameter_filename, "msgpack.xz")
                    output_filepath = datadir("curie", shape_str, savename(parameter_directory), output_filename)
                    let output_directory = dirname(output_filepath)
                        isdir(output_directory) || mkpath(output_directory)
                    end        
                    open(output_filepath, "w") do io
                        ioc = XzCompressorStream(io)
                        MsgPack.pack(ioc, save_data)
                        close(ioc)
                    end
                end # for temperature
            end # for pii, pic
        end # for tii
    end # for qn
end # function compute



function parse_shape(shape_str::AbstractString)
    shape_pattern = r"\(\s*([-+]?\d+)\s*,\s*([-+]?\d+)\s*\)x\(\s*([-+]?\d+)\s*,\s*([-+]?\d+)\s*\)"
    m = match(shape_pattern, shape_str)
    if isnothing(m)
        throw(ArgumentError("shape should be in format (n1a,n1b)x(n2a,n2b)"))
    end
    n1a, n1b, n2a, n2b = [parse(Int, x) for x in m.captures]
    return [n1a n2a;
            n1b n2b]
end


function shape_string(shape::AbstractMatrix{<:Integer})
    n1a = shape[1,1]
    n1b = shape[2,1]
    n2a = shape[1,2]
    n2b = shape[2,2]
    return "($n1a,$n1b)x($n2a,$n2b)"
end



function parse_commandline()
    s = ArgParse.ArgParseSettings()
    @add_arg_table! s begin
        "shape"
            arg_type = String
            help = "shape of the lattice in the format (?,?)x(?,?)"
            required = true
        "--hopping", "-t"
            arg_type = Float64
            default = 1.0
        "--interaction", "-U"
            arg_type = Float64
            required = true
        "--temperature", "-T"
            arg_type = Float64
            nargs = '+'
            required = true
        "--n", "-n"
            arg_type = Int
            nargs = '*'
            range_tester = x -> x >= 0
        "--Sz"
            arg_type = Float64
            nargs = '*'
            range_tester = x -> x == 0.5 * round(2*x)
        "--tii"
            arg_type = Int
            nargs = '*'
        "--pii"
            arg_type = Int
            nargs = '*'
        "--pic"
            arg_type = Int
            nargs = '*'
        "--maxdense"
            arg_type = Int
            default = 20000
            range_tester = x -> x > 10
        "--krylovdim"
            arg_type = Int
            default = 200
            range_tester = x -> x > 0
        "--nsamples", "-R"
            arg_type = Int
            default = 1000
            range_tester = x -> x > 0
        "--seed"
            arg_type = Int
            required = true
        "--force", "-f"
            action = :store_true
        "--debug", "-d"
            action = :store_true
    end
    parse_args(s)
end


function main()
    parsed_args = parse_commandline()
    if parsed_args["debug"]
        logger = ConsoleLogger(stdout, Logging.Debug; meta_formatter=my_metafmt)
        global_logger(logger)
    else
        logger = ConsoleLogger(stdout, Logging.Info; meta_formatter=my_metafmt)
        global_logger(logger)
    end

    shape = parse_shape(parsed_args["shape"])
    t = parsed_args["hopping"]
    U = parsed_args["interaction"]
    temperatures = parsed_args["temperature"]
    n_list = parsed_args["n"]
    Sz_list = parsed_args["Sz"]
    tii_list = parsed_args["tii"]
    pii_list = parsed_args["pii"]
    pic_list = parsed_args["pic"]
    maxdense = parsed_args["maxdense"]
    krylovdim = parsed_args["krylovdim"]
    nsamples = parsed_args["nsamples"]
    seed = parsed_args["seed"]
    force = parsed_args["force"]


    shape_str = shape_string(shape)
    df = CSV.read(datadir("sectors-$shaoe_str.csv"), DataFrames)
    

    compute(
        shape,
        t,
        U,
        temperatures,
        n_list,
        Sz_list,
        tii_list,
        pii_list,
        pic_list,   
        ;
        maxdense=maxdense,
        krylovdim=krylovdim,
        nsamples=nsamples,
        seed=seed,
        force=force,
    )
end


main()

