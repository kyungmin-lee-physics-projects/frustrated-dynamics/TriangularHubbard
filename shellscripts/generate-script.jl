using DrWatson
@quickactivate "TriangularHubbard"

import SQLite
import DBInterface as DBI
using DataFrames


function main()
    max_array_size=300

    hopping=1.0
    interaction=20.0
    temperatures=[0.04, 0.06, 0.08, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0]

    # shape_str="(2,-2)x(2,4)"
    shape_str = "(3,0)x(0,3)"
    conn = DBI.connect(SQLite.DB, datadir("curie", shape_str, "sectors.sqlite3"))
    df_schedule = DataFrame(DBI.execute(conn, "SELECT * FROM schedule"))
    df_dense = DataFrame(DBI.execute(conn, "SELECT * FROM dense_results"))
    df_sparse = DataFrame(DBI.execute(conn, "SELECT * FROM sparse_results"))
    DBI.close!(conn)

    df_dense  = df_dense[(df_dense[:, :hopping] .== hopping) .& (df_dense[:, :interaction] .== interaction), :]
    df_sparse = df_sparse[(df_sparse[:, :hopping] .== hopping) .& (df_sparse[:, :interaction] .== interaction), :]
    
    dense_indices = Int[]
    for row in eachrow(df_schedule)
        rowmissing = false
        for T in temperatures
            if row.idx ∉ df_dense[df_dense[:, :temperature] .== T, :idx]
                rowmissing = true
                break
            end
            rowmissing && break
        end
        if rowmissing
            push!(dense_indices, row.idx)
        end
    end
    @show dense_indices
end

main()
