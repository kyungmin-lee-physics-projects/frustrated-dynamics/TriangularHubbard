using NLsolve
using Statistics
using StatsBase

MARKER_CYCLE = ["o", "x", "s", "^"]

function read_lattice_data(latticefilepath::AbstractString)
    jsondata = open(latticefilepath, "r") do f
        JSON.parse(read(f, String))
    end

    shape = hcat(jsondata["shape"]...)
    n_sites = Int(det(shape))
    bravaiscoordinates = hcat(jsondata["bravais_coordinates"]...)
    reciprocallatticevectors = hcat(jsondata["unitcell"]["reciprocallatticevectors"]...)
    sitecoordinates = hcat((x["cartesian_coordinates"] for x in jsondata["supercell"]["sites"])...)
    momentums = hcat((x["cartesian_coordinates"] for x in jsondata["momentums"])...)
    fractionalmomentums = hcat(([eval(Meta.parse(y)) for y in x["fractional_coordinates"]] for x in jsondata["momentums"])...)
    sitegroupmap = Dict()
    for (i, vs) in enumerate(jsondata["equivalent_site_pairs"])
        for v in vs
            sitegroupmap[v] = i
        end
    end
    phases = cis.(-2π * momentums' * sitecoordinates)
    n_sitegroup = maximum(values(sitegroupmap))
    
    p0 = [4π/3, 0]
    R = [cos(π/3) -sin(π/3); sin(π/3) cos(π/3)]
    zone_boundary = [p0]
    p = p0
    for i in 1:5
        p = R * p
        push!(zone_boundary, p)
    end
    push!(zone_boundary, p0)
    zone_boundary = hcat(zone_boundary...);    
    
    latticedata = (
        shape=shape,
        n_sites=n_sites,
        bravaiscoordinates=bravaiscoordinates,
        reciprocallatticevectors=reciprocallatticevectors,
        sitecoordinates=sitecoordinates,
        momentums=momentums,
        fractionalmomentums=fractionalmomentums,
        phases=phases,
        sitegroupmap=sitegroupmap,
        n_sitegroup=n_sitegroup,
        zone_boundary=zone_boundary
    )
    return latticedata
end

function getrealspacemap(data::AbstractVector, latticedata)
    realspacemap = fill(NaN, latticedata.n_sites)
    @assert length(data) == latticedata.n_sitegroup
    for i in 1:latticedata.n_sites
        realspacemap[i] = data[latticedata.sitegroupmap[[1,i]]]
    end
    return realspacemap
end


function read_data(
        sqlfile ::AbstractString,
        kpoint_indices::AbstractDict{<:AbstractString, <:Integer},
    )
    conn = DBInterface.connect(SQLite.DB, sqlfile)
    df = DataFrame(DBInterface.execute(conn, """
            SELECT
              sectors.idx, nup, ndn, tii, pii, pic, dim, 
              hopping, interaction, temperature,
              base_energy, partition, energy, energy_squared, correlation, 'dense' as type
            FROM dense_results
            JOIN sectors USING(idx)
            JOIN dense_energy_shifts USING(idx, hopping, interaction)


            UNION

            SELECT
              sectors.idx, nup, ndn, tii, pii, pic, dim, 
              hopping, interaction, temperature,
              base_energy, partition, energy, energy_squared, correlation, 'sparse' as type
            FROM sparse_results
            JOIN sectors USING(idx)
            JOIN sparse_energy_shifts USING(idx, hopping, interaction)
            ;        
        """))
    close(conn)
    return df
end

function process_data!(
        df::AbstractDataFrame,
        latticedata
    )
    n_sites = latticedata.n_sites
#     hopping = only(unique(df.hopping))
#     interaction = only(unique(df.interaction))
   
    df[!, :charge] = (df[!, :nup] + df[!, :ndn])
    df[!, :charge2] = (df[!, :nup] + df[!, :ndn]).^2
    df[!, :density] = df[!, :charge] ./ n_sites
    df[!, :Sz] = 0.5 .* (df[!, :nup] - df[!, :ndn])
    df[!, :Sz2] = df[!, :Sz].^2
    
    rmaps = Dict{String, Vector{Vector{Float64}}}("uu"=> [], "ud" => [], "du" => [], "dd" => [])
    for s in df[!, :correlation]
        m = JSON.parse(s)
        replace!(m, nothing=>NaN)
        push!(rmaps["uu"], getrealspacemap(m[1:4:end], latticedata))
        push!(rmaps["ud"], getrealspacemap(m[2:4:end], latticedata))
        push!(rmaps["du"], getrealspacemap(m[3:4:end], latticedata))
        push!(rmaps["dd"], getrealspacemap(m[4:4:end], latticedata))
    end
    rmaps = Dict(k=> hcat(v...) for (k, v) in rmaps)
    kmaps = Dict(k => latticedata.phases * v for (k, v) in rmaps)
    
    kmaps["SzSz"] = 0.25 * (kmaps["uu"] - kmaps["ud"] - kmaps["du"] + kmaps["dd"])
    df[!, :Sq] .= eachcol(kmaps["SzSz"])
    
    for (kname, kindex) in kpoint_indices
        for opname in ["uu", "ud", "du", "dd", "SzSz"]
            df[!, "$(opname)_$(kname)"] = real.(kmaps[opname][kindex,:] .* n_sites);
        end
    end 
end


function check_data(
        df::AbstractDataFrame
    )
    @show filter(row -> !isapprox(row.uu_GM, row.nup.^2 * row.partition), df)
    
    for (irow, row) in enumerate(eachrow(df))
        x = row.uu_GM
        y = row.nup .* row.nup .* row.partition
        if !isapprox(x, y)
            println("$(irow)\t$(row.idx)\t$(row.temperature)\t$(x)\t$(y)\t$(x - y)\t$(row.type)")
        end
    end
    
    for (irow, row) in enumerate(eachrow(df))
        x = row.dd_GM
        y = row.ndn .* row.ndn .* row.partition
        if !isapprox(x, y)
            println("$(irow)\t$(row.idx)\t$(row.temperature)\t$(x)\t$(y)\t$(x - y)\t$(row.type)")
        end
    end
    
    for (irow, row) in enumerate(eachrow(df))
        x = row.ud_GM
        y = row.ndn .* row.nup .* row.partition
        if !isapprox(x, y)
            println("$(irow)\t$(row.idx)\t$(row.temperature)\t$(x)\t$(y)\t$(x - y)\t$(row.type)")
        end
    end
    
end


# function process(df)
#     df[!, :Sq] .= eachcol(kmaps["SzSz"])
# end

function checkplot(df)
    # Comparing two methods of obtaining susceptibility

    for fdf in groupby(df, [:hopping, :interaction])
        out = Dict()
        for gdf in groupby(fdf, [:charge, :temperature]; sort=true)
            min_base_energy = minimum(gdf.base_energy)
            q = first(gdf.charge)
            T = first(gdf.temperature)
            boltzmann = exp.(-(gdf.base_energy .- min_base_energy) ./ T)
            susc1 = sum(gdf.Sz2 .* boltzmann .* gdf.partition) / sum(boltzmann .* gdf.partition) / T
            susc2 = sum(gdf.SzSz_GM .* boltzmann) / sum(boltzmann .* gdf.partition) / T
            if !haskey(out, q)
                out[q] = []
            end
            push!(out[q], [T, susc1, susc2])
        end
        for q in keys(out)
            out[q] = hcat(out[q]...)
        end

        fig = PyPlot.figure(figsize=(4, 2))
        ax1 = fig.add_subplot(1,1,1)
#         ax2 = fig.add_subplot(1,2,2)
        for charge in sort(collect(keys(out)))
            susceptibility = out[charge]
            ax1.plot(susceptibility[1,15:end], susceptibility[2,15:end] - susceptibility[2,15:end], label="$charge")
#             ax1.plot(susceptibility[1,15:end], susceptibility[2,15:end], label="$charge")
#             ax2.plot(susceptibility[1,15:end], susceptibility[3,15:end], label="$charge")
        end
    end
    # PyPlot.legend()
end


function plot_specific_heat(
        canonical_df::AbstractDataFrame,
        nrows::Integer,
        ncols::Integer;
        prefix::AbstractString=""
    )
    fig = PyPlot.figure(figsize=(5, 5))
#     fig.subplots_adjust(hspace=0, wspace=0)

    axs = fig.subplots(nrows, ncols, sharex=true, sharey=false)
    axs_transpose = permutedims(axs, [2,1])

    for (ax, sdf) in zip(axs_transpose, groupby(canonical_df, :density))
        sdf = sort(sdf, :temperature)
        density = first(sdf.density)
        xs = sdf.temperature
        ys = sdf.specific_heat
        
        xs, ys = let i = .! isnan.(ys) .& .! isinf.(ys)
            xs[i], ys[i]
        end
        
        ax.plot(xs, ys, color="k", linewidth=1, linestyle="-")
        ax.axhline(0, linewidth=0.5, color="k")
        ax.axvline(0, linewidth=0.5, color="k")
        ax.text(
            0.2, 0.9, (@sprintf "\$n=%.2f\$" density),
            transform=ax.transAxes,
            ha="left", va="top",
            fontsize="x-small"
        )
        ax.set_ylim(0, percentile(ys, 95))
#         ax.set_ylim(0, 4*median(ys[.! isnan.(ys) .& .! isinf.(ys)] ))
    end

    for i in 1:nrows
        axs[i, 1].set_ylabel(raw"$C_v$")
    end
    for i in 1:ncols
        axs[end, i].set_xlabel(raw"$T/t$")
        axs[end, i].set_xticks([0,2,4])
    end
    axs[1,1].set_xlim(0, 6)
#     axs[1,1].set_ylim(0, 4)

    fig.savefig("$(prefix)specific-heat_$(shape_str).pdf", dpi=300, bbox_inches="tight")

    fig = PyPlot.figure(figsize=(5, 5))
#     fig.subplots_adjust(hspace=0, wspace=0)

    ax = nothing
    axs = fig.subplots(nrows, ncols, sharex=true, sharey=false)
    axs_transpose = permutedims(axs, [2,1])

    for (ax, sdf) in zip(axs_transpose, groupby(canonical_df, :density))
        sdf = sort(sdf, :temperature)
        density = first(sdf.density)

        xs = sdf.temperature
        ys = sdf.specific_heat ./ sdf.temperature
                
        xs, ys = let i = .! isnan.(ys) .& .! isinf.(ys)
            xs[i], ys[i]
        end
        
        ax.plot(xs, ys, color="k", linewidth=1, linestyle="-")
        ax.text(
            0.9, 0.05, (@sprintf "\$n=%.2f\$" density),
            transform=ax.transAxes,
            ha="right", va="bottom",
            fontsize="x-small",
    #         bbox=Dict("facecolor"=>"white", "edgecolor"=>"None", "alpha"=>0.5),
        )
    #     ax.axvline(1/20, linestyle="--")
        ax.set_ylim(0, percentile(ys, 95))
    end

    for i in 1:nrows
        axs[i, 1].set_ylabel(raw"$C_v/T$")
    end
    for i in 1:ncols
        axs[end, i].set_xlabel(raw"$T/t$")
        axs[end, i].set_xticks([0, 1, 2, ])
    end
    axs[1,1].set_xlim(0, 2.3)
#     axs[1,1].set_ylim(0, 10)

    fig.savefig("$(prefix)entropy_$(shape_str).pdf", dpi=300, bbox_inches="tight")    
    
    
end


function compute_canonical(
        df::AbstractDataFrame,
        kpoint_indices::AbstractDict
    )
    n_sites = latticedata.n_sites
    
    ts = Float64[]
    Us = Float64[]
    Qs = Int[]
    Ts = Float64[]
    χs = Dict{String, Vector{Float64}}(k => Float64[] for k in keys(kpoint_indices))
    χs["0"] = Float64[]
    Sqs = Vector{Float64}[]
    Cvs = Float64[]
    for sdf in groupby(df, [:hopping, :interaction, :charge, :temperature])
        t = first(sdf.hopping)
        U = first(sdf.interaction)
        Q = first(sdf.charge)
        T = first(sdf.temperature)
        
        E0 = minimum(sdf.base_energy)
        boltzmann = exp.(-(sdf.base_energy .- E0) ./ T)
        Z = sum(boltzmann .* sdf.partition)
        push!(ts, t)
        push!(Us, U)
        push!(Qs, Q)
        push!(Ts, T)
        push!(χs["0"], sum(sdf.Sz2 .* boltzmann .* sdf.partition) / Z / T / n_sites)
        for kname in keys(kpoint_indices)
            push!(χs[kname], sum(sdf[!, "SzSz_$kname"] .* boltzmann) / Z / T / n_sites)
        end
                
        Sq = zeros(Float64, length(sdf[1, :Sq]))
        for (row, z) in zip(eachrow(sdf), boltzmann)
            Sq += row.Sq .* z
        end
        Sq ./= Z
        push!(Sqs, real.(Sq))
        
        let
            corr = sum(
                (sdf.energy_squared
                    + 2 .* sdf.base_energy .* sdf.energy
                    + (sdf.base_energy.^ 2) .* sdf.partition) .* boltzmann    
            ) / Z
            avg = sum((sdf.energy .+ sdf.base_energy .* sdf.partition) .* boltzmann) / Z
            push!(Cvs, (corr - avg^2) / (n_sites * T^2))
        end
    end
    canonical_df = DataFrame(
        "hopping" => ts,
        "interaction" => Us,
        "charge" => Qs,
        "density" => Qs ./ n_sites,
        "temperature" => Ts,
        "specific_heat" => Cvs,
        "susceptibilities_0" => χs["0"],
        ("susceptibilities_$kname" => χs[kname] for kname in keys(kpoint_indices))...,
        "Sq" => Sqs,
    )    
    sort!(canonical_df, [:charge, :temperature])
    return canonical_df
end



function compute_canonical_fit(
        canonical_df::AbstractDataFrame,
        model,
        temperature_range::AbstractVector{<:Real},
        latticedata,
    )
    n_sites = latticedata.n_sites
    hoppings = Float64[]
    interactions = Float64[]
    densities = Float64[]
    
    susceptibility_names = filter(startswith("susceptibilities_"), names(canonical_df))
    knames = [split(s, "_")[2] for s in susceptibility_names]
    
    intercepts = Dict(kname => Float64[] for kname in knames)
    slopes = Dict(kname => Float64[] for kname in knames)
    
    for sdf in groupby(canonical_df, [:interaction, :hopping, :density])
#         if any(
#             any(x -> isinf(x) || isnan(x), 1.0 ./ sdf[!, s])
#             for s in susceptibility_names
#         )
#             continue
#         end
        push!(hoppings, first(sdf.hopping))
        push!(interactions, first(sdf.interaction))
        push!(densities, first(sdf.density))
        for k in knames
            xs = sdf.temperature
            ys = 1 ./ sdf[!, "susceptibilities_$k"]
            ys = ys[ temperature_range[1] .<= xs .<= temperature_range[2]]
            xs = xs[ temperature_range[1] .<= xs .<= temperature_range[2]]
            f = curve_fit(model, xs, ys, [1.0, 0.0])
            push!(intercepts[k], f.param[2])
            push!(slopes[k], f.param[1])
        end
    end
    canonical_fit_df = DataFrame(
        "hopping" => hoppings,
        "interaction" => interactions,
        "density" => densities,
        ("intercept_$k" => intercepts[k] for k in knames)...,
        ("slope_$k" => slopes[k] for k in knames)...,
    )
    return canonical_fit_df
end


function plot_susceptibility(
        canonical_df::AbstractDataFrame,
        canonical_fit_df::AbstractDataFrame,
        panel_shape=nothing;
        prefix::AbstractString="",
        xlim=[-1.0, 6.0],
        ylim=[-0.5, 80.],
    )
    knames = [split(s, "_")[2] for s in filter(startswith("susceptibilities_"), names(canonical_df))]
    nrows, ncols = panel_shape

    canonical_df = sort(canonical_df, :density)
    canonical_fit_df = sort(canonical_fit_df, :density)
    
    for fdf in groupby(canonical_df, [:hopping, :interaction])
        t = only(unique(fdf.hopping))
        U = only(unique(fdf.interaction))
        for kname in knames
            fig = PyPlot.figure(figsize=(5, 5))
            fig.subplots_adjust(hspace=0, wspace=0)
            axs = fig.subplots(nrows, ncols, sharex=true, sharey=true)
            axs_transpose = permutedims(axs, [2,1])

            density_ax = Dict()
            for (ax, sdf) in zip(axs_transpose, groupby(fdf, :density))
                density = first(sdf.density)
                density_ax[density] = ax
                sdf = sort(sdf, :temperature)

                xs = sdf.temperature
                ys = inv.(sdf[!, "susceptibilities_$kname"])
                ax.plot(xs, ys, color="k", linewidth=1, linestyle="-")
                ax.axhline(0, linewidth=0.5, color="k")
                ax.axvline(0, linewidth=0.5, color="k")
            end
            for row in eachrow(filter(row->row.hopping==t && row.interaction == U, canonical_fit_df))
                density = row.density
                ax = density_ax[density]
                xs_fit = [-1, 10]
                ys_fit = model(xs_fit, [row["slope_$kname"], row["intercept_$kname"]])

                ax.plot(xs_fit, ys_fit, linewidth=1, color="xkcd:red", linestyle="--")
                ax.text(
                    0.2, 0.9, (@sprintf "\$n=%.2f\$\n\$\\Theta/t=%.3f \$" density row["intercept_$kname"]),
                    transform=ax.transAxes,
                    ha="left", va="top",
                    fontsize="x-small"
                )
            end

            for i in 1:nrows
                axs[i, 1].set_ylabel("\$1/\\chi\$")
            end
            for i in 1:ncols
                axs[end, i].set_xlabel(raw"$T/t$")
#                 axs[end, i].set_xticks([0,2,4,6])
            end
            axs[1,1].set_xlim(xlim...)
            axs[1,1].set_ylim(ylim...)

            fig.suptitle("$kname")
#             fig.savefig("$(prefix)susceptibility-$(kname)_$(shape_str).pdf", dpi=300, bbox_inches="tight")
            fig.savefig(savename("$(prefix)susceptibility-$(kname)_$(shape_str)", Dict("t"=>t, "U"=>U), "pdf"), dpi=300, bbox_inches="tight")
        end
    end
end

FANCY_MOMENTUM_NAMES = Dict(
    "GM" => raw"$\Gamma$",
    "SM" => raw"$\Sigma$",
    "LD" => raw"$\Lambda$",
    "M" => raw"$M$",
    "K" => raw"$K$",
)


function plot_susceptibility_normalized(
        canonical_df::AbstractDataFrame,
#         canonical_fit_df::AbstractDataFrame,
        knames::AbstractVector{<:AbstractString},
        styles::AbstractDict,
        panel_shape=nothing;
        prefix::AbstractString="",
    )
#     knames = [split(s, "_")[2] for s in filter(startswith("susceptibilities_"), names(canonical_df))]
    nrows, ncols = panel_shape

    for fdf in groupby(canonical_df, [:hopping, :interaction])
        t = only(unique(fdf.hopping))
        U = only(unique(fdf.interaction))
        
        cdf = sort(fdf, :density)
    #     canonical_fit_df = sort(canonical_fit_df, :density)

        fig = PyPlot.figure(figsize=(5, 5))
        fig.subplots_adjust(hspace=0, wspace=0)
        axs = fig.subplots(nrows, ncols, sharex=true, sharey=true)
        axs_transpose = permutedims(axs, [2,1])

        density_ax = Dict()
        for (ax, sdf) in zip(axs_transpose, groupby(cdf, :density))
            density = first(sdf.density)
            density_ax[density] = ax
            ax.text(
                0.9, 0.9, (@sprintf "\$n=%.2f\$" density),
                transform=ax.transAxes,
                ha="right", va="top",
                fontsize="x-small"
            )
            ax.axhline(1, linestyle="-", linewidth=0.5, color="k")
        end

        for kname in knames
            (kname == "0" || kname == "GM") && continue
            for (ax, sdf) in zip(axs_transpose, groupby(cdf, :density))
                density = first(sdf.density)
                density_ax[density] = ax
                sdf = sort(sdf, :temperature)

                xs = sdf.temperature
                ys = sdf[!, "susceptibilities_$kname"] ./ sdf.susceptibilities_0
                ax.plot(xs, ys,
                    linewidth=1,
                    color=styles[kname].color,
                    linestyle=styles[kname].linestyle,      
                    label=styles[kname].label)
            end
        end

        for i in 1:nrows
            axs[i, 1].set_ylabel("\$\\chi/\\chi_{\\Gamma}\$")
        end
        for i in 1:ncols
            axs[end, i].set_xlabel(raw"$T/t$")
            axs[end, i].set_xticks([0,2,4,6])
        end
        axs[1,1].set_xlim(0, 6)
        axs[1,1].set_ylim(0.5, 2.1)
    #     axs[1,1].set_yscale("log")
    #     axs[1,1].set_ylim(0.8, 2.1)

        axs[1,end].legend(loc=4)
    #     fig.suptitle("$kname")
        fig.savefig(savename("$(prefix)susceptibility-normalized_$(shape_str)", Dict("t"=>t, "U"=>U), "pdf"), dpi=300, bbox_inches="tight")
#         fig.savefig("$(prefix)susceptibility-normalized_$(shape_str).pdf", dpi=300, bbox_inches="tight")
    end
end


function plot_curie_weiss_fit(canonical_fit_df, kname, ylim1=(-1.5, 0.5), ylim2=(0, 2);
    prefix::AbstractString="")
    fig = PyPlot.figure(figsize=(3.5, 4.5))
    ax1 = fig.add_subplot(2,1,1)
    ax2 = fig.add_subplot(2,1,2, sharex=ax1)
    for (i, sdf) in enumerate(groupby(canonical_fit_df, [:hopping, :interaction]))
        t = only(unique(sdf.hopping))
        U = only(unique(sdf.interaction))
        ax1.plot(sdf.density, sdf[!, "intercept_$kname"], marker=MARKER_CYCLE[i], label="t=$t, U=$U", alpha=0.5)
        ax2.plot(sdf.density, sdf[!, "slope_$kname"], marker=MARKER_CYCLE[i], label="t=$t, U=$U", alpha=0.5)
    end
    ax1.axhline(0, color="black", linewidth=0.5, linestyle="-")
    ax1.set_ylabel(raw"$\Theta/t$")
    ax1.set_ylim(ylim1...)
    ax1.legend()

    ax2.set_ylim(ylim2...)
    ax2.set_ylabel(raw"$C$")
    ax2.set_xlabel(raw"$n$")
    fig.suptitle(kname)
    fig.savefig("$(prefix)curie-weiss-fit-$(kname)_$(shape_str).pdf", dpi=300, bbox_inches="tight")
end


# function compute_structure_factor_canonical(df::AbstractDataFrame)
#     ρs = Float64[]
#     Ts = Float64[]
#     Sqs = Vector{Float64}[]
    
#     for sdf in groupby(df, [:density, :temperature])
#         T = first(sdf.temperature)
#         ρ = first(sdf.density) 
#         Sq = zeros(Float64, length(sdf[1, :Sq]))
#         Z = 0.0
#         for row in eachrow(sdf)
#             Sq += row.Sq
#             Z += row.partition
#         end
#         Sq ./= Z
#         push!(ρs, ρ)
#         push!(Ts, T)
#         push!(Sqs, real.(Sq))
#     end
#     structure_factor_df = DataFrame("density" => ρs, "temperature" => Ts, "structure_factor" => Sqs)
#     return structure_factor_df
# end



# function compute_structure_factor_grandcanonical(df::AbstractDataFrame)
#     ρs = Float64[]
#     Ts = Float64[]
#     Sqs = Vector{Float64}[]
    
#     for sdf in groupby(df, [:temperature])
#         T = first(sdf.temperature)
#         ρ = first(sdf.density) 
#         Sq = zeros(Float64, length(sdf[1, :Sq]))
#         Z = 0.0
#         for row in eachrow(sdf)
#             Sq += row.Sq
#             Z += row.partition
#         end
#         Sq ./= Z
#         push!(ρs, ρ)
#         push!(Ts, T)
#         push!(Sqs, real.(Sq))
#     end
#     structure_factor_df = DataFrame("density" => ρs, "temperature" => Ts, "structure_factor" => Sqs)
#     return structure_factor_df
# end



function plot_Sq(
        Sq::AbstractVector{<:Real},
        latticedata,
        ax::PyObject;
        marker::AbstractString="o",
        markersize::Integer=200,
        inverse::Bool=false)
    k0 = latticedata.momentums .* 2π
    G = latticedata.reciprocallatticevectors
    
    k = Matrix{Float64}(undef, (2, 0))
    v = Vector{Float64}(undef, 0)
    if inverse
        Sq = -inv.(Sq)
    end
    for R1 in [-1,0,1], R2 in [-1,0,1]
        R = [R1; R2]
        k = hcat(k, broadcast(+, k0, G * R))
        v = vcat(v, Sq)
    end
    if inverse
        minv = minimum(filter(!isnan, v) )
        maxv = maximum(filter(!isnan, v) )
        c = mpl.cm.viridis((v .- minv) ./ (maxv - minv))
    else
        minv = minimum(filter(!isnan, v) )
        maxv = maximum(filter(!isnan, v) )
        c = mpl.cm.viridis(v ./ maxv)
    end
    img = ax.scatter(k[1,:], k[2,:], s=markersize, c=c, marker=marker)
    ax.plot(latticedata.zone_boundary[1,:], latticedata.zone_boundary[2,:], color="red")
    ax.set_xlim(-5,5)
    ax.set_ylim(-5,5)
    ax.set_xticks([])
    ax.set_yticks([])
    ax.set_aspect(1.0)
    return img
end



function plot_structure_factor_map(
        canonical_df,
        latticedata,
        nrows::Integer, ncols::Integer;
        marker::AbstractString="o",
        markersize::Integer=200,
        prefix=""
    )
    fig = PyPlot.figure(figsize=(6,6))

    for gdf in groupby(canonical_df, [:hopping, :interaction])
        t = only(unique(gdf.hopping))
        U = only(unique(gdf.interaction))
        pdf = backend_pdf.PdfPages(savename("$(prefix)static-structure-factor_$(shape_str)", Dict("t"=>t, "U"=>U), "pdf"))
        @showprogress for sdf in groupby(gdf, :temperature)
            fig.clf()
            axs = fig.subplots(nrows, ncols, sharex=true, sharey=true)
            axs_transpose = permutedims(axs, [2,1])

            T = first(sdf.temperature)
            img = nothing
            for (ax, row) in zip(axs_transpose, eachrow(sdf))
                density = row.density
                Sq = copy(row.Sq)
                img = plot_Sq(Sq, latticedata, ax; marker=marker, markersize=markersize)
                ax.set_title((@sprintf "\$n=%.3f\$" density), fontsize="small")
            end
            cbar_ax = fig.add_axes([0.92, 0.15, 0.02, 0.7])
            cbar = fig.colorbar(img, cax=cbar_ax, ticks=[0,1])
            cbar_ax.set_yticklabels(["0", "max"])        

            fig.suptitle((@sprintf "\$T=%.2f\$" T), fontsize="medium")
            pdf.savefig(fig, dpi=300, bbox_inches="tight")
        end
        let d = pdf.infodict()
            d["Title"] = "Static Structure Factor"
            d["Author"] = "Kyungmin Lee <kyungmin.lee.42@gmail.com>"
            d["Subject"] = "Shape = $shape_str"
            d["CreationDate"] = string(today())
        end
        pdf.close()
    end
end



# function plot_structure_factor_map_nogamma(
#         canonical_df,
#         latticedata,
#         nrows::Integer, ncols::Integer;
#         marker::AbstractString="o",
#         markersize::Integer=200,
#         prefix=""
#     )
#     fig = PyPlot.figure(figsize=(6,6))

#     pdf = backend_pdf.PdfPages("$(prefix)static-structure-factor-nogamma_$(shape_str).pdf")
#     @showprogress for sdf in groupby(canonical_df, :temperature)
#         fig.clf()
#         axs = fig.subplots(nrows, ncols, sharex=true, sharey=true)
#         axs_transpose = permutedims(axs, [2,1])

#         T = first(sdf.temperature)
#         img = nothing
#         for (ax, row) in zip(axs_transpose, eachrow(sdf))
#             density = row.density
#             Sq = copy(row.Sq)
#             Sq[1] = NaN
#             img = plot_Sq(Sq, latticedata, ax; marker=marker, markersize=markersize)
#             ax.set_title((@sprintf "\$n=%.3f\$" density), fontsize="small")
#         end
#         cbar_ax = fig.add_axes([0.92, 0.15, 0.02, 0.7])
#         cbar = fig.colorbar(img, cax=cbar_ax, ticks=[0,1])
#         cbar_ax.set_yticklabels(["0", "max"])        

#         fig.suptitle((@sprintf "\$T=%.2f\$" T), fontsize="medium")
#         pdf.savefig(fig, dpi=300, bbox_inches="tight")
#     end
#     let d = pdf.infodict()
#         d["Title"] = "Static Structure Factor"
#         d["Author"] = "Kyungmin Lee <kyungmin.lee.42@gmail.com>"
#         d["Subject"] = "Shape = $shape_str"
#         d["CreationDate"] = string(today())
#     end
#     pdf.close()
# end



function compute_grand_canonical(
        df::AbstractDataFrame,
        latticedata,
        kpoint_indices,
        target_densities::AbstractVector{<:Real}=0.2:0.1:1.7
    )
    n_sites = latticedata.n_sites
    ρs = Float64[]
    converged_ρs = Float64[]
    μs = Float64[]
    ts = Float64[]
    Us = Float64[]
    Ts = Float64[]
    χs = Dict{String, Vector{Float64}}(k => Float64[] for k in keys(kpoint_indices))
    χs["0"] = Float64[]
    Sqs = Vector{Float64}[]
    Cvs = Float64[]
    for sdf in groupby(df, [:hopping, :interaction, :temperature])
        t = only(unique(sdf.hopping))
        U = only(unique(sdf.interaction))
        T = only(unique(sdf.temperature))
        for ρ in target_densities
            μ = NaN
            for trial in 1:100
                function f!(F, x)
                    μ = x[1]
                    E0 = minimum(sdf.base_energy .- μ .* sdf.charge)
                    boltzmann = exp.(-(sdf.base_energy .- μ .* sdf.charge .- E0 ) ./ T)
                    Z = sum(boltzmann .* sdf.partition)
                    F[1] = sum(sdf.charge .* boltzmann .* sdf.partition) / Z / n_sites - ρ
                end
                y = nlsolve(f!, [5*rand()]; autodiff=:forward, iterations=20000)
                μ = y.zero[1]
                if converged(y)
                    break
                end
            end

            T = first(sdf.temperature)
            E0 = minimum(sdf.base_energy .- μ .* sdf.charge)
            boltzmann = exp.(-(sdf.base_energy .- μ .* sdf.charge .- E0 ) ./ T)

            Z = sum(boltzmann .* sdf.partition)
            
            push!(ρs, ρ)
            
            push!(ts, t)
            push!(Us, U)
            push!(Ts, T)
            push!(μs, μ)
            push!(converged_ρs, sum(sdf.charge .* boltzmann .* sdf.partition) / sum(boltzmann .* sdf.partition) / n_sites)
            push!(χs["0"], sum(sdf.Sz2 .* boltzmann .* sdf.partition) / Z / T / n_sites)
            for kname in keys(kpoint_indices)
                push!(χs[kname], sum(sdf[!, "SzSz_$kname"] .* boltzmann) / Z / T / n_sites)
            end
            
            Sq = zeros(Float64, length(sdf[1, :Sq]))
            for (row, z) in zip(eachrow(sdf), boltzmann)
                Sq += row.Sq .* z
            end
            Sq ./= Z
            push!(Sqs, real.(Sq))
            
            let
                corr = sum(
                    (sdf.energy_squared
                        .+ 2 .* (sdf.base_energy .- μ .* sdf.charge) .* sdf.energy
                        .+ ((sdf.base_energy .- μ .* sdf.charge) .^ 2) .* sdf.partition) .* boltzmann
#                     (sdf.energy_squared
#                         .+ 2 .* sdf.base_energy .* sdf.energy
#                         .+ sdf.base_energy.^ 2 .* sdf.partition) .* boltzmann                    
                ) / Z
                avg = sum((sdf.energy .+ (sdf.base_energy .- μ .* sdf.charge ) .* sdf.partition) .* boltzmann) / Z
#                 avg = sum((sdf.energy .+ sdf.base_energy .* sdf.partition) .* boltzmann) / Z
                push!(Cvs, (corr - avg^2) / (n_sites * T))
            end
        end
    end
    grand_canonical_df = DataFrame(
        "density" => ρs,
        "converged_density" => converged_ρs,
        "chemical_potential" => μs,
        "hopping" => ts,
        "interaction" => Us,
        "temperature" => Ts,
        "specific_heat" => Cvs,
        "susceptibilities_0" => χs["0"],
        ("susceptibilities_$kname" => χs[kname] for kname in keys(kpoint_indices))...,
        "Sq" => Sqs,
    )
    sort!(grand_canonical_df, [:density, :temperature])
    return grand_canonical_df
end

#         let
#             corr = sum(
#                 (sdf.energy_squared
#                     + 2 .* sdf.base_energy .* sdf.energy
#                     + (sdf.base_energy.^ 2) .* sdf.partition) .* boltzmann    
#             ) / Z
#             avg = sum((sdf.energy .+ sdf.base_energy .* sdf.partition) .* boltzmann) / Z
#             push!(Cvs, (corr - avg^2) / (n_sites * T))
#         end
