BEGIN;

.separator | ' '

PRAGMA temp_store = 2;

CREATE TEMP TABLE maxcount_all (count INTEGER);

INSERT INTO maxcount_all
  SELECT max(mycount) AS count FROM (
    SELECT count(1) AS mycount
    FROM dense_results
    GROUP BY idx
    -- UNION
    -- SELECT count(1) AS mycount
    -- FROM sparse_results
    -- GROUP BY idx
  );
.print "maxcount"
SELECT * FROM maxcount_all;
.print ''

-- Small
.print 'small'
CREATE TEMP TABLE remaining_dense (idx INTEGER PRIMARY KEY, dim INTEGER);

INSERT INTO remaining_dense
  SELECT schedule.idx AS idx, dim
  FROM schedule
  LEFT JOIN sectors ON schedule.idx = sectors.idx
  WHERE schedule.idx NOT IN (
    SELECT idx FROM (
        SELECT idx, COUNT(1) AS mycount FROM dense_results
        GROUP BY idx)
    WHERE mycount = (SELECT count FROM maxcount_all)
  ) AND type='small' AND dim > 0
;
SELECT count(1) FROM remaining_dense;
.print ''
SELECT idx FROM remaining_dense ORDER BY dim;

DROP TABLE remaining_dense;

.print ''

-- Dense
.print 'dense'
CREATE TEMP TABLE remaining_dense (idx INTEGER PRIMARY KEY, dim INTEGER);

INSERT INTO remaining_dense
  SELECT idx, dim
  FROM schedule
  JOIN sectors USING(idx)
  WHERE idx NOT IN (
    SELECT idx FROM (
        SELECT idx, COUNT(1) AS mycount FROM dense_results
        GROUP BY idx)
    WHERE mycount = (SELECT count FROM maxcount_all)
  ) AND type='dense' AND dim > 0
;
SELECT count(1) FROM remaining_dense;
.print ''
SELECT idx FROM remaining_dense ORDER BY dim;

DROP TABLE remaining_dense;

.print ''

-- Sparse
.print 'sparse'
CREATE TEMP TABLE remaining_sparse (idx INTEGER PRIMARY KEY, dim INTEGER);

-- INSERT INTO remaining_sparse
--   SELECT schedule.idx AS idx, dim
--   FROM schedule
--   LEFT JOIN sectors ON schedule.idx = sectors.idx
--   WHERE schedule.idx NOT IN (
--     SELECT idx FROM (
--         SELECT idx, COUNT(1) AS mycount FROM sparse_results
--         AND samplecount >= 500
--         GROUP BY idx)
--     WHERE mycount = (SELECT count FROM maxcount_all)
--   ) AND type='sparse' AND dim > 0
-- ;
INSERT INTO remaining_sparse
  SELECT idx, dim
  FROM schedule
  JOIN sectors USING(idx)
  WHERE idx NOT IN (
    SELECT idx FROM sparse_results
    GROUP BY idx, hopping, interaction, temperature
    HAVING sum(samplecount) >= 500
  ) AND type='sparse' AND dim > 0
;
SELECT count(1) FROM remaining_sparse;
.print ''
SELECT idx FROM remaining_sparse ORDER BY dim;
-- SELECT idx,dim FROM remaining_sparse ORDER BY dim;

DROP TABLE remaining_sparse;

END;
